
import {fetchAPI} from '../utils/fetch';

export default {
  createReport(body) {
    return new Promise((resolve, reject) => {
      fetchAPI(`/report`, 'POST', body)
        .then((response) => {
          return response
        })
        .then((response) => {
          resolve(response);
        });
    })
  },
}

