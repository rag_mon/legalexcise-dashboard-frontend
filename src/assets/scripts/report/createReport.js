import * as $ from 'jquery';
import Vue from 'vue'
import 'vue-datetime/dist/vue-datetime.css'
import {DateTime as LuxonDateTime} from 'luxon'
import {Datetime} from 'vue-datetime';
import API from './api';

export default function ($table) {
  console.log('createReport');
  const $btnCreateReport = $('#btnCreateReport');
  const $modal = $('#modalCreateReport');
  const $modalSubmitMessage = $('#modalSubmitMessage');
  const lexicon = {

    RU: {
      modalSubmit: {
        form: {
          labelName: 'Название отчета',
          labelFrom: 'От',
          labelTo: 'До',
          btnClose: 'Закрыть',
          btnAdd: 'Создать'
        },

        validation: {
          name: `Название отчета обязательно для заполнения `,
          date: `Пожалуйста укажите дату либо От либо Дою.`
        },
        success: {
          title: 'Успех',
          message: 'Отчет создан.',
        },
        error: {
          title: 'Ошибка',
          message: `Произошла непредвиденная ошибка. Попробуйте перезагрузить страницу и повторить запрос.`,
        },
      }

    },
    UK: {
      modalSubmit: {
        form: {
          labelName: 'Назва звіту',
          labelFrom: 'Від',
          labelTo: 'До',
          btnClose: 'Закрити',
          btnAdd: 'Створити'
        },
        validation: {
          name: `Назва звіту обов'язково для заповнення`,
          date: `ласка вкажіть дату або Від або Дою.`
        },
        success: {
          title: 'Успіх',
          message: 'Звіт створено.',
        },
        error: {
          title: 'Помилка',
          message: `Сталася неочікувана помилка. Спробуйте перезавантажити сторінку і повторити запит.`,
        },
      }
    }
  };

  let currentLocal = ( window.config && window.config.lang) || 'RU';
  currentLocal = currentLocal.toUpperCase();

  if (currentLocal !== 'RU' || currentLocal !== 'UK') {
    currentLocal = 'RU';
  }
  // Vue.use(Datetime)
  const FilterOfChart = new Vue({
    el: '#createReportDataPicker',
    template: `
      <div>
        <div class="modal-body">
          <div class="row">
           <div class="col-md-12 form-group">
               <label>${lexicon[currentLocal].modalSubmit.form.labelName}</label>
               <input type="text" name="name" class="form-control" v-model="name"/>
               <div class="invalid-feedback"></div>
           </div>
           <div class="col-md-6 form-group">
             <label>${lexicon[currentLocal].modalSubmit.form.labelFrom}:</label>
             <datetime input-class="form-control" type="datetime" :disabled="disabled" v-model="from_dt"></datetime>
           </div>
           <div class="col-md-6 form-group">
             <label>${lexicon[currentLocal].modalSubmit.form.labelTo}:</label>
             <datetime input-class="form-control" type="datetime" :disabled="disabled" v-model="to_dt"></datetime>
           </div>
           <div class="col-md-12 form-group">
            <div class="invalid-feedback" style="display: block;">
              {{message}}
            </div>
           </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">${lexicon[currentLocal].modalSubmit.form.btnClose}</button>
          <button type="button" v-on:click="submit" class="btn btn-success btn-add">${lexicon[currentLocal].modalSubmit.form.btnAdd}</button>
        </div>
      </div>
     
    `,
    components: {
      datetime: Datetime
    },
    data: {
      name: null,
      from_dt: null,
      to_dt: null,
      disabled: false,
      message: null,
    },
    watch: {},
    methods: {
      submit: function () {
        // this.disabled = true;
        let validationDate = [false, false];
        if (!this.name) {
          this.message = lexicon[currentLocal].modalSubmit.validation.name;
          return
        }

        if (this.from_dt) {
          validationDate.pop()
        }
        if (this.to_dt) {
          validationDate.pop()
        }
        if (validationDate.length === 2) {
          this.message = lexicon[currentLocal].modalSubmit.validation.date;
          return
        }
        this.message = null;

        console.log('create report name: ', this.name);
        console.log('create report from_dt: ', this.from_dt);
        console.log('create report to_dt: ', this.to_dt);
        console.log('create report from_dt: ', LuxonDateTime.fromISO(this.from_dt).toFormat('dd.MM.yyyy HH:mm:ss'));
        console.log('create report to_dt: ', LuxonDateTime.fromISO(this.to_dt).toFormat('dd.MM.yyyy HH:mm:ss'));
        $modal.modal('hide');

        try {
          API.createReport({
            name: this.name,
            ...(this.from_dt ? {from_dt: LuxonDateTime.fromISO(this.from_dt).toFormat('dd.MM.yyyy HH:mm:ss')} : null),
            ...(this.to_dt ? {to_dt: LuxonDateTime.fromISO(this.to_dt).toFormat('dd.MM.yyyy HH:mm:ss')} : null),
          }).then((respons) => {
            console.log(respons.toString() === 'TypeError: Failed to fetch');
            if (respons.toString() === 'TypeError: Failed to fetch') {
              return Promise.reject(new Error(respons));
            }

            this.name = null;
            this.from_dt = null;
            this.to_dt = null;
            $modalSubmitMessage.find('#modalSubmitMessageLabel').html(lexicon[currentLocal].modalSubmit.success.title);
            $modalSubmitMessage.find('.data-message-value').html(lexicon[currentLocal].modalSubmit.success.message);
            $modalSubmitMessage.modal('show');

          }).catch((error) => {
            console.log(error);
            this.name = null;
            this.from_dt = null;
            this.to_dt = null;
            $modalSubmitMessage.find('#modalSubmitMessageLabel').html(lexicon[currentLocal].modalSubmit.error.title);
            $modalSubmitMessage.find('.data-message-value').html(lexicon[currentLocal].modalSubmit.error.message);
            $modalSubmitMessage.modal('show');

          })
        } catch (error) {
          console.log(error);
          this.name = null;
          this.from_dt = null;
          this.to_dt = null;
          $modalSubmitMessage.find('#modalSubmitMessageLabel').html(lexicon[currentLocal].modalSubmit.error.title);
          $modalSubmitMessage.find('.data-message-value').html(lexicon[currentLocal].modalSubmit.error.message);
          $modalSubmitMessage.modal('show');
        }


      }
    }
  });

}

