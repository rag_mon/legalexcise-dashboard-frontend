import * as $ from 'jquery';
import dateFilter from '../datatable/plugins/dateFilter';
import DataTableAPI from '../datatable/api';
import moment from 'moment';
import createReport from './createReport';

export default (function () {

  const lexicon = {
    RU: {
      ready: 'Отчет готов',
      not_ready: 'Отчет не готов',
      rendering: 'Отчет обрабатывается',
      status_unknown: 'Статус неизвестен',
    },
    UK: {
      ready: 'Звіт готовий',
      not_ready: 'Звіт не готовий',
      rendering: 'Звіт обробляється',
      status_unknown: 'Статус невідомий',
    },
  };

  let currentLocal = ( window.config && window.config.lang) || 'RU';
  currentLocal = currentLocal.toUpperCase();

  if (currentLocal !== 'RU' || currentLocal !== 'UK') {
    currentLocal = 'RU';
  }


  // DATA SORT
  dateFilter($, moment);
  $.fn.dataTable.moment('DD.MM.YYYY HH:mm');

  const $addressListTable = $('#reportListTable')
    .on('init.dt', function () {
      console.log('Table initialisation complete');
      setTimeout(() => {
        $addressListTable.draw();
      }, 1000)
    })
    .DataTable({
      buttons: [
        'delete'
      ],
      "drawCallback": function () {
        console.log('drawCallback');

      },
      stateSave: false,
      ordering: true,
      info: true,
      scrollX: true,
      "processing": true,
      "serverSide": true,
      ...(process.env.NODE_ENV === 'development' ? {
        "ajax": "http://localhost:4000/report",
      } : {
        "ajax": "/report",
      }),
      "columns": [
        {
          "render": function (data, type, row) {
            console.log(row);
            try {
              //return row.data.user.name;
                return (row.user_id ? row.user_id: '-');
            } catch (err) {
              console.error(err);
              return '-';
            }
          }
        },
        {
          "render": function (data, type, row) {
            return row.name;
          }
        },
        {
          "render": function (data, type, row) {
            switch (row.status) {

              case('ready'): {
                return lexicon[currentLocal].ready
              }
              case('not_ready'): {
                return lexicon[currentLocal].not_ready
              }
              case('rendering'): {
                return lexicon[currentLocal].rendering
              }
              default: {
                return lexicon[currentLocal].status_unknown
              }

            }
          }
        },
        {
          "render": function (data, type, row) {
            return row.created_at;
          }
        },
        {
          "render": function (data, type, row) {
            // заменено: created_at -> fixed_at
            if (row.status === 'ready') {
              return `
                <a href="/report/download/${row.id}" class="btn btn-primary">
                  Скачать отчет
                </a>`;
            } else {
              return `
                <a href="#!" class="btn btn-light">
                  Отчет не готов
                </a>`;
            }


          }
        },
      ],

      language: {
        url: DataTableAPI.getLanguageUrl(),
      }
    });

  $('#btnCreateReport').on('click', function () {
    createReport($addressListTable);
  })
}());
