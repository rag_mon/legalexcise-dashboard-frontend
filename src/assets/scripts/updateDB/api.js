import {fetchAPI} from '../utils/fetch';


export default {
  updatesList() {
    return new Promise((resolve, reject) => {
      fetchAPI(`/updates`)
        .then((response) => {
          return response.json()
        })
        .then((response) => {
          resolve(response);
        })
        .catch((err) => {
          reject(err);
        });
    })
  }
}
