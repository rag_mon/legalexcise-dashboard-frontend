import * as $ from 'jquery';
import DataTableAPI from '../datatable/api';
import API from './api';

export default (function () {
  const lexicon = {

    RU: {
      button: {
        updates: 'Издать обновление',
      },
      modalSubmit:{
        success: {
          title: 'Успех',
          message:'Обновление издано.',
        },
        error:  {
          title: 'Ошибка',
          message:'Обновление не издано.',
        },
      }

    },
    UK: {
      button: {
        updates: 'Видати оновлення',
      },
      modalSubmit:{
        success: {
          title: 'Успіх',
          message:'Оновлення видано.',
        },
        error:  {
          title: 'Помилка',
          message:'Оновлення не видано.',
        },
      }
    }
  };
  let currentLocal = ( window.config && window.config.lang) || 'RU';
  currentLocal = currentLocal.toUpperCase();

  if (currentLocal !== 'RU' || currentLocal !== 'UK') {
    currentLocal = 'RU';
  }
  const $updatesListTable = $('#updatesListTable')
    .on('init.dt', function () {
      console.log('Table initialisation complete');
      $('#updatesListTable_wrapper').prepend(`<div class="peer"><button style="margin-bottom: 1rem;" id="button-update-db" type="button" class="btn cur-p btn-primary">${lexicon[currentLocal].button.updates}</button></div>`);

      $('#button-update-db').on('click', updateDB);

      setTimeout(() => {
        $updatesListTable.draw();
      }, 1000)
    })
    .DataTable({
      "drawCallback": function () {
        console.log('drawCallback');
      },
      stateSave: false,
      ordering: false,
      info: false,
      scrollX: true,
      "searching": false,
      "processing": true,
      "serverSide": true,
      ...(process.env.NODE_ENV === 'development'? {
        "ajax": "http://localhost:4000/updates",
      }:{
        "ajax": "/updates",
      }),
      "columns": [
        {
          "render": function (data, type, row) {
            return row.id;
          }
        },
        {
          "render": function (data, type, row) {
            return row.timestamp;
          }
        }
      ],
      "paging": false,
      language: {
        url: DataTableAPI.getLanguageUrl(),
      }
    });


  const updateDB = () => {
    const $modalUpdateDBConfirm = $('#modalUpdateDBConfirm');
    const $modalSubmitMessage = $('#modalSubmitMessage');

    $modalUpdateDBConfirm.modal({
      show: true,
    });

    $modalUpdateDBConfirm.find('.btn-updates').unbind('click').bind('click', function () {
      $modalUpdateDBConfirm.modal('hide');
      API.updatesList()
        .then((response) => {
          console.log(response);
          $modalSubmitMessage.find('#modalSubmitMessageLabel').html(lexicon[currentLocal].modalSubmit.success.title);
          $modalSubmitMessage.find('.data-message-value').html(lexicon[currentLocal].modalSubmit.success.message);
          $modalSubmitMessage.modal({
            show: true,
          });
        })
        .catch((error) => {
          console.log(error);
          $modalSubmitMessage.find('#modalSubmitMessageLabel').html(lexicon[currentLocal].modalSubmit.error.title);
          $modalSubmitMessage.find('.data-message-value').html(lexicon[currentLocal].modalSubmit.error.message);
          $modalSubmitMessage.modal({
            show: true,
          });
        })

    })

  }


}())
