import * as $ from 'jquery';
import loadGoogleMapsAPI from 'load-google-maps-api';
import API from './api';
import {AddressesCluster} from "./AddressesCluster";

export default (function () {
  if ($('#google-map').length > 0) {

    loadGoogleMapsAPI({
      key: 'AIzaSyDW8td30_gj6sGXjiMU0ALeMu1SDEwUnEA',
    }).then(() => {
      const latitude = 46.484583;
      const longitude = 30.7326;
      const mapZoom = 7;
      const {google} = window;

      const mapOptions = {
        center: new google.maps.LatLng(latitude, longitude),
        zoom: mapZoom,
        scrollwheel: true,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
      };

      window.map = new google.maps.Map(document.getElementById('google-map'), mapOptions);

    });

  }
  if ($('#google-map-addresses').length > 0) {

    loadGoogleMapsAPI({
      key: 'AIzaSyDW8td30_gj6sGXjiMU0ALeMu1SDEwUnEA',
    }).then(() => {
      const latitude = 46.484583;
      const longitude = 30.7326;
      const mapZoom = 7;
      const {google} = window;

      const mapOptions = {
        center: new google.maps.LatLng(latitude, longitude),
        zoom: mapZoom,
        scrollwheel: true,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
      };

      const MAP = new google.maps.Map(document.getElementById('google-map-addresses'), mapOptions);
      window.MAP_ADDRESSES = MAP;

      API.getAdresses()
        .then((response) => {
          console.log(response);

          const ADDRESSES_CLUSTER = new AddressesCluster(MAP, response);
        })
        .catch((err) => {
          console.log(err);
        })


    });

  }
}())
