export const lexicon = {
  'RU': {
    company_desc: {
      title: 'Информация о лицензии',
      type: 'Тип',
      company: 'Название',
      license_type: 'Тип лицензии',
      license_number: 'Номер лицензии',
      license_start_at: 'Дата начала',
      license_end_at: 'окончания',
      measures: 'Принятые меры',
      confiscated_goods: "Конфискованная продукция",
      protocol_drawn_up: "Составлен протокол от",
      financial_sanctions: "Применена финансовая санкция",
      alcohol: 'Алкоголь',
      tobacco: 'Табачная',
      beer: 'Алкоголь (только пиво)',
      report_abuse: 'Сообщить о нарушении',
    },

  },
  'UK': {
    company_desc: {
      title: 'Інформація про ліцензії',
      type: 'Тип',
      company: 'Назва',
      license_type: 'Тип ліцензії',
      license_number: 'Номер ліцензії',
      license_start_at: 'Дата початку',
      measures: 'Вжиті заходи',
      confiscated_goods: 'Вилучена продукція',
      protocol_drawn_up: 'Складено протокол від',
      financial_sanctions: 'Застосовані фінансови санкції',
      license_end_at: 'закінчення',
      alcohol: 'Алкогольна',
      tobacco: 'Тютюнова',
      beer: 'Алкоголь (тільки пиво)',
      report_abuse: 'Повідомити про порушення',
    },
  }
}
