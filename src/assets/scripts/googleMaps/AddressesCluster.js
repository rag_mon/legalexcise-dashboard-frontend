import './markerclusterer';
import * as $ from 'jquery';
import {lexicon} from './lexicon';
import marker_license_active from './marker_license_active.png';
import marker_license_canceled from './marker_license_canceled.png';
export class AddressesCluster {

  static ADDRESSES;
  static MAP;
  MARLERS = [];
  MARLERS_CANCALED = [];

  constructor(map, addresses) {
    this.ADDRESSES = addresses;
    this.MAP = map;
    this.onClickCluster = this.onClickCluster.bind(this);
    this.init = this.init.bind(this);
    this.init();

  }

  init() {
    const {google} = window;
    console.log(marker_license_active);
    console.log(marker_license_canceled);
    this.ADDRESSES.map((item) => {
      if (item.lng && item.lat) {
        if (item.license_type !== 'mixed') {

          let marker = new google.maps.Marker({
            position: new google.maps.LatLng(item.lat, item.lng),
            addressInfo: item,
            icon: marker_license_active
          });
          marker.addListener('click', this.onClickMarker(item));
          this.MARLERS.push(marker);
        } else {
          let marker = new google.maps.Marker({
            position: new google.maps.LatLng(item.lat, item.lng),
            addressInfo: item,
            icon: marker_license_canceled
          });
          marker.addListener('click', this.onClickMarker(item));
          this.MARLERS_CANCALED.push(marker);
        }

      }
    });

    let options = {
      gridSize: 60,
      maxZoom: 15,
      // imagePath: 'assets/static/images/gmaps/m'
    };
    let markerCluster = new MarkerClusterer(this.MAP, this.MARLERS, {...options,className: 'cluster_license--active'});
    let markerClusterCanceled = new MarkerClusterer(this.MAP, this.MARLERS_CANCALED,  {...options,className: 'cluster_license--canceled'});

    google.maps.event.addListener(markerCluster, 'clusterclick', this.onClickCluster);
    google.maps.event.addListener(markerClusterCanceled, 'clusterclick', this.onClickCluster);

  }

  onClickCluster(cluster) {
    let markers = cluster.getMarkers();
    console.log('onClickCluster cluster', cluster);
    console.log('onClickCluster markers', markers);

    if (markers && markers.length < 20) {
      console.log('onMarkerClusterClick: ', markers);

      console.log(this.positionCheck(markers));
      if (this.positionCheck(markers)) {
        console.log('onMarkerClusterClick positionCheck: ', markers);
        const addressInfoArray = [];
        markers.map((item, index) => {
          addressInfoArray.push(item.addressInfo);
        });
        console.log('onMarkerClusterClick addressInfoArray: ', addressInfoArray);

        this.createModalWindow(addressInfoArray);
      }

    }

  }

  onClickMarker(address) {
    return (event) => {
      console.log('click event', event);
      console.log('click address', address);

      this.createModalWindow([address]);

    }
  }

  createModalWindow(data) {
    let $modal = $('#modalAddressMarkerMap');
    let htmlArray = [];
    let currentLocal = ( window.config && window.config.lang) || 'RU';
    currentLocal = currentLocal.toUpperCase();

    if (currentLocal !== 'RU' || currentLocal !== 'UK') {
      currentLocal = 'RU';
    }
    $modal.find('.modal-body').empty();

    $('#modalAddressMarkerMapLabel').text(lexicon[currentLocal].company_desc.title);
    data.map((item) => {
      htmlArray.push(this.render(item));
    });

    $modal.find('.modal-body').append(htmlArray);

    $modal.modal({
      show: true
    })

  }

  positionCheck(clickedMarkers) {
    const length = clickedMarkers.length;
    const searchParam = {
      lng: clickedMarkers[0].position.lng(),
      lat: clickedMarkers[0].position.lat(),
    };
    for (let i = 1; i < length; i++) {
      if (searchParam.lng !== clickedMarkers[i].position.lng() && searchParam.lat !== clickedMarkers[i].position.lat()) {
        return false
      }
    }
    return searchParam
  }


  render(item) {
    let currentLocal = ( window.config && window.config.lang) || 'RU';
    currentLocal = currentLocal.toUpperCase();

    return (`
      <div>                
          <table class="table">
           <thead class="thead-default">
              <tr>
                <th style="background-color: #0275d8!important; color: #fff;" colspan="2">
                  <h4 style="margin: 0;">${lexicon[currentLocal].company_desc.company}: ${item.company}</h4>
                </th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <th scope="row">${lexicon[currentLocal].company_desc.type}</th>
                <td>${item.company_type}</td>
              </tr>
              <tr>
                <th scope="row">${lexicon[currentLocal].company_desc.license_type}</th>
                <td>${item.license_type === 'alcohol' ? lexicon[currentLocal].company_desc.alcohol : lexicon[currentLocal].company_desc.tobacco}</td>
              </tr>
              <tr>
                <th scope="row">${lexicon[currentLocal].company_desc.license_number}</th>
                <td>${item.license}</td>
              </tr>
              <tr>
                <th scope="row">${lexicon[currentLocal].company_desc.license_start_at}/${lexicon[currentLocal].company_desc.license_end_at}</th>
                <td>${item.license_start_at} — ${item.license_end_at}</td>
              </tr>
              
              
            </tbody>
          </table>
          <br/>
          <table class="table">
            <thead class="thead-default">
              <tr>
                <th style="background-color: #eceeef;" colspan="2">${lexicon[currentLocal].company_desc.measures}</th>
              </tr>
            </thead>
            <tbody>              
              <tr>
                <th style="width: 50%;">${lexicon[currentLocal].company_desc.confiscated_goods}</th>
                <td>${item.public_notices && item.public_notices.confiscated_goods}</td>
              </tr>
              <tr>
                <th style="width: 50%;">${lexicon[currentLocal].company_desc.protocol_drawn_up}</th>
                <td>${item.public_notices && item.public_notices.protocol_drawn_up}</td>
              </tr>
              <tr>
                <th style="width: 50%;">${lexicon[currentLocal].company_desc.financial_sanctions}</th>
                <td>${item.public_notices && item.public_notices.financial_sanctions}</td>
              </tr>
            </tbody>
           </table> 
          <br/>   
      </div>      
    `)
  }

}
