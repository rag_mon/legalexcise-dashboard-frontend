export default {

  getLanguageCode() {
    switch ((window.config && window.config.lang) || window.navigator.language.match(/([A-z]{2})/i)[1]) {
      case 'uk': return 'Ukrainian';
      case 'ru': return 'Russian';
      default:
        return 'Russian';
    }
  },

  getLanguageUrl() {
    return '//cdn.datatables.net/plug-ins/1.10.16/i18n/' + this.getLanguageCode() + '.json';
  }

}