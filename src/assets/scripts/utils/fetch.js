export const fetchAPI = (url, method, body) => {

  const END_POINT = process.env.NODE_ENV === 'development' && url.indexOf('http') < 0  ? 'http://localhost:4000' : '';

  function status(response) {
    if (response.status >= 200 && response.status < 300) {
      return Promise.resolve(response)
    } else {
      return Promise.reject(new Error(response.statusText))
    }
  }

  function json(response) {
    return response.json()
  }

  return fetch(END_POINT + url, {
    method: method || 'post',
    credentials: process.env.NODE_ENV === 'development' ? 'omit' : 'include',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',

      ...(window.config && window.config.lang ? {
        'Accept-Language': window.config && window.config.lang,
      } : null),

      ...($('meta[name="csrf-token"]').length ? {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
      } : null)

    },
    ...(body ? {body: JSON.stringify(body)} : null)
  })
    .then(status)
    .catch((err) => {
      return err;
    });
};
