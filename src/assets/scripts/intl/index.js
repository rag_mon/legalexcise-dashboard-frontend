import * as $ from 'jquery';
import API from './api';

export default (function () {
  let currentLocal = ( window.config && window.config.lang) || 'RU';
  currentLocal = currentLocal.toUpperCase();

  if (currentLocal !== 'RU' || currentLocal !== 'UK') {
    currentLocal = 'RU';
  }
  console.log(process.env.NODE_ENV);
  API.getIntlList()
    .then((res) => {
      initSelect(res);
    })
    .catch((res) => {
      console.log(res);
      if (process.env.NODE_ENV === 'development') {
        initSelect(["ru", "uk"]);
      }
    });


  const initSelect = (data) => {
    // <option>...</option>
    let optArr = [];
    if (!data.length) {
      if (process.env.NODE_ENV === 'development') {
        data = ["ru", "uk"]
      } else {
        return
      }
    }
    data.map((item) => {
      if (currentLocal === item.toUpperCase()) {
        optArr.push(`<option selected>${item}</option>`)
      } else {
        optArr.push(`<option>${item}</option>`)
      }
    });

    $('#intlToggle').append(optArr);
    $('#intlToggle').on('change', function (e) {
      API.changeLang($(this).val())
        .then((res) => {
          console.log(res);
        })
        .catch((res) => {
          console.log(res);
        })
    })
  };


}())
