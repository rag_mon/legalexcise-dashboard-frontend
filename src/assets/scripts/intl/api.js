import {fetchAPI} from '../utils/fetch';

export default {
  getIntlList() {
    return new Promise((resolve, reject) => {
      fetchAPI(`/lang_list`, 'get')
        .then((response) => {
          return response.json()
        })
        .then((response) => {
          resolve(response);
        })
        .catch((err) => {
          reject(err);
        });
    })
  },
  changeLang(lang) {
    return new Promise((resolve, reject) => {
      fetchAPI(`/change_lang`, 'POST',JSON.stringify({lang}))
        .then((response) => {
          return response.json()
        })
        .then((response) => {
          resolve(response);
        })
        .catch((err) => {
          reject(err);
        });
    })
  },

}
