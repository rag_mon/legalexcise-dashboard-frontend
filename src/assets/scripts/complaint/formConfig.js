import {
  required,
  isNumber,
  Latitude,
  Longtude,
  maxLength,
} from '../address/validation';

export const formConfig = [

  {
    // address — nullable|string;
    id: 'address',
    name: 'address',
    required: false,
    validate: []
  },
  {
    // address_id — nullable|integer|exists:addresses,id;
    id: 'address_id',
    name: 'address_id',
    required: false,
    validate: [isNumber]
  },
  {
// comment — string|max:10000;
    id: 'comment',
    name: 'comment',
    required: false,
    validate: [maxLength(10000)]
  },  {
    // company — required|string|max:255;
    id: 'company',
    name: 'company',
    required: true,
    validate: [required, maxLength(255)]
  },
  {
    // dfs_department_code — nullable|string|max:191;
    id: 'dfs_department_code',
    name: 'dfs_department_code',
    required: false,
    validate: [maxLength(191)]
  },
  {
    // dfs_executor — nullable|string|max:191;
    id: 'dfs_executor',
    name: 'dfs_executor',
    required: false,
    validate: [maxLength(191)]
  },
  {
    // email — nullable|email;
    id: 'email',
    name: 'email',
    required: false,
    validate: [maxLength(255)]
  },
  {
    // lat — nullable|numeric;
    id: 'lat',
    name: 'lat',
    required: false,
    validate: []
  },
  {
    // lng — nullable|numeric;
    id: 'lng',
    name: 'lng',
    required: false,
    validate: []
  },
  {
    // message — required|string;
    id: 'message',
    name: 'message',
    required: true,
    validate: [required]
  },
  {
    // name — nullable|string|max:255;
    id: 'name',
    name: 'name',
    required: false,
    validate: [maxLength(255)]
  },
  {
    // np_code — nullable|string|max:255;
    id: 'np_code',
    name: 'np_code',
    required: false,
    validate: [maxLength(255)]
  },
  {
    // protocol — nullable|string;
    id: 'protocol',
    name: 'protocol',
    required: false,
    validate: []
  },
  {
// spd_address — string|max:191;
    id: 'spd_address',
    name: 'spd_address',
    required: false,
    validate: [maxLength(191)]
  },
  {
// spd_code — string|max:191;
    id: 'spd_code',
    name: 'spd_code',
    required: false,
    validate: [maxLength(191)]
  },
  {
// spd_license — string|max:191;
    id: 'spd_license',
    name: 'spd_license',
    required: false,
    validate: [maxLength(191)]
  },
  {
// spd_name — string|max:191;
    id: 'spd_name',
    name: 'spd_name',
    required: false,
    validate: [maxLength(191)]
  },
  {
    // status — nullable|in:processed,not_processed,transferred_to_oy,verification_possible,verification_impossible,in_processing;
    id: 'status',
    name: 'status',
    required: false,
    validate: []
  },
  {
    // telephone — nullable|string|max:255;
    id: 'telephone',
    name: 'telephone',
    required: false,
    validate: [maxLength(255)]
  },

  {
    // to_code —nullable|string|max:255 ;
    id: 'to_code',
    name: 'to_code',
    required: false,
    validate: [maxLength(255)]
  },
  {
    // type — required|string;
    id: 'type',
    name: 'type',
    required: true,
    validate: [required]
  },
  {
    // verified_at — nullable|date_format:d.m.Y;
    id: 'verified_at',
    name: 'verified_at',
    required: false,
    validate: []
  },
  {
    // violations - nullable|string (склеивание массива значений указанных чекбоксов в строку через разделитель ",", аналогично по этому же разделителю при рендеринге страницу управления нужно производить установку статуса "выбрано" для списка чекбокса);
    id: 'violations',
    name: 'violations',
    required: false,
    validate: []
  },

];


