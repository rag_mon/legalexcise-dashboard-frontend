// complaintListTable
import DataTableAPI from '../datatable/api';
import deleteComplaint from './deleteComplaint';
import addressToComplaint from './addressToComplaint';
import npCode from './npCode';
import toCode from './toCode';
import status from './status';
import confiscatedGoods from './confiscated_goods';
import protocolDrawnUp from './protocol_drawn_up';
import financialSanctions from './financial_sanctions';
import dfsDepartmentCode from './dfs_department_code';
import dfsExecutor from './dfs_executor';

export default (function () {
    if ($('#complaintListTable').length) {

        const lexicon = {

            RU: {
                dropdawn: {
                    action: 'Действие',
                    codeNp: 'Указать код НП',
                    codeTo: 'Указать код ТО',
                    dfs_executor: 'Исполнитель ДФС',
                    dfs_department_code: 'Номер жалобы',
                    gotToMap: 'Перенести на карту',
                    delete: 'Удалить',
                    confiscated_goods: 'Добавить кол-во конфискованого товара',
                    protocol_drawn_up: 'Указать дату составления протокола',
                    financial_sanctions: 'Добавить финансовую санкцию',
                    show: 'Просмотреть',
                    edit: 'Управление'
                },

            },
            UK: {
                dropdawn: {
                    action: 'Дія',
                    codeNp: 'Вказати код НП',
                    codeTo: 'Вказати код ТО',
                    dfs_executor: 'Виконавець ДФС',
                    dfs_department_code: 'Номер жалобы',
                    gotToMap: 'Перенести на карту',
                    delete: 'Видалити',
                    confiscated_goods: 'Додати кількість конфіскованого товару',
                    protocol_drawn_up: 'Вказати дату складання протоколу',
                    financial_sanctions: 'Додати фінансову санкцію',
                    show: 'Просмотреть',
                    edit: 'Управление'
                },
            }
        };

        let currentLocal = ( window.config && window.config.lang) || 'RU';
        currentLocal = currentLocal.toUpperCase();

        if (currentLocal !== 'RU' || currentLocal !== 'UK') {
            currentLocal = 'RU';
        }


        const $complaintListTable = $('#complaintListTable')
            .DataTable({
                    buttons: [
                        'delete'
                    ],
                    paging: true,
                    searching: true,
                    stateSave: true,
                    ordering: true,
                    info: true,
                    scrollX: true,
                    "processing": true,
                    "serverSide": true,
                    ...(process.env.NODE_ENV === 'development' ? {
                        "ajax": "http://localhost:4000/complaints",
                    } : {
                        "ajax": "/complaint",
                    }),
                // "ajax": "/complaint",

                // "ajax": {
                //   "type" : "GET",
                //   "url" : "http://localhost:4000/complaints",
                //   "dataSrc": function ( json ) {
                //     //Make your callback here.
                //     console.log(json);
                //     alert("Done!");
                //     return json.data;
                //   }
                // },
                "drawCallback": function () {
            console.log('drawCallback');


            deleteComplaint($complaintListTable);
            addressToComplaint($complaintListTable);
            npCode($complaintListTable);
            toCode($complaintListTable);
            confiscatedGoods($complaintListTable);
            protocolDrawnUp($complaintListTable);
            financialSanctions($complaintListTable);
            dfsDepartmentCode($complaintListTable);
            dfsExecutor($complaintListTable);
            status();
        },
        "createdRow": function (row,data,type) {
            if (data.danger) {
                row.classList.add('table-danger');
            }
        },
        "columns": [
            {
                "data": "created_at",
                "searchable": true,
                "orderable": true,
                "render": function (data,type,row) {
                    return row.created_at;
                },
                "filter": function (data,type,row) {return row.created_at;}
            },
            {
                "data": "address",
                "searchable": true,
                "orderable": true,
                "render": function (data,type,row) {
                    return row.address;
                }
            },
            {
                "data": "company",
                "searchable": true,
                "orderable": true,
                "render": function (data,type,row) {
                    return row.company;
                }
            },
            {
                "data": "message",
                "searchable": true,
                "orderable": true,
                "render": function (data,type,row) {
                    return row.message;
                }
            },
            {
                "data": "np_code",
                "searchable": true,
                "orderable": true,
                "render": function (data,type,row) {
                    return row.np_code;
                }
            },
            {
                "data": "to_code",
                "searchable": true,
                "orderable": true,
                "render": function (data,type,row) {
                    return row.to_code;
                }
            },
            {
                "data": "dfs_department_code",
                "searchable": true,
                "orderable": true,
                "render": function (data,type,row) {
                    return row.dfs_department_code || '-';
                }
            },
            {
                "data": "dfs_executor",
                "searchable": true,
                "orderable": true,
                "render": function (data,type,row) {
                    // ..исполнитель ДФС
                    return row.dfs_executor || '-';
                }
            },
            {
                "render": function (data,type,row) {
                    // status

                    return `
              <select class="form-control" data-complaint-id="${row.id}" name="status">                
                ${row.status === null || row.status === 'not_processed' ? '<option selected value="null">Выберите...</option>' : '<option value="null">Выберите...</option>'}
                ${row.status === 'verification_possible' ? '<option selected value="verification_possible">Возможно проведение проверки</option>' : '<option value="verification_possible">Возможно проведение проверки</option>'}  
                ${row.status === 'verification_impossible' ? '<option selected value="verification_impossible">Невозможность проведения проверки</option>' : '<option value="verification_impossible">Невозможность проведения проверки</option>'}  
                ${row.status === 'processed' ? '<option selected value="processed">Проведена проверка</option>' : '<option value="processed">Проведена проверка</option>'}  
                ${row.status === 'transferred_to_oy' ? '<option selected value="transferred_to_oy">Передано в ОУ</option>' : '<option value="transferred_to_oy">Передано в ОУ</option>'}     
                ${row.status === 'in_processing' ? '<option selected value="in_processing">Обрабатывается</option>' : '<option value="in_processing">Обрабатывается</option>'}           
              </select>`
                }
            },
            {
                "render": function (data,type,row) {
                    return row.confiscated_goods;

                }
            },
            {
                "render": function (data,type,row) {
                    return row.protocol_drawn_up;

                }
            },
            {
                "render": function (data,type,row) {
                    return `${row.financial_sanctions} грн`;
                }
            },
            {
                "render": function (data,type,row) {
                    return `
                <div class="dropdown">
                  <button class="btn btn-primary dropdown-toggle" type="button"
                        id="dropdownMenuButton"
                        data-toggle="dropdown" aria-haspopup="true"
                        aria-expanded="false">
                    ${lexicon[ currentLocal ].dropdawn.action}
                  </button>
                  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                  
                    <!--show-->
                    <a
                      class="dropdown-item dropdown-item-show"
                      href="/complaint/${row.id}"
                      onclick=""
                    >
                      ${lexicon[ currentLocal ].dropdawn.show}
                    </a>
                    
                    <!--control/edit-->
                    <a
                      class="dropdown-item dropdown-item-edit"
                      href="/complaint/${row.id}/edit"
                      onclick=""
                    >
                      ${lexicon[ currentLocal ].dropdawn.edit}
                    </a>
                  
                    <!--np_code-->
                    <a
                      data-toggle="modal"
                      data-target="#modalAddNpCode"
                      class="dropdown-item dropdown-item-add-np-code"
                      href="#"
                      onclick=""
                      data-complaint-id="${row.id}"
                      data-np-code="${row.np_code}"
                    >
                      ${lexicon[ currentLocal ].dropdawn.codeNp}
                    </a>
                    
                    <!--to_code-->
                    <a
                      data-toggle="modal"
                      data-target="#modalAddToCode"
                      class="dropdown-item dropdown-item-add-to-code"
                      href="#"
                      data-complaint-id="${row.id}"
                      data-to-code="${row.to_code}"
                    >
                      ${lexicon[ currentLocal ].dropdawn.codeTo}
                    </a>
                    
                    <!--dfs_department_code-->
                    <a
                      data-toggle="modal"
                      data-target="#modalComplaintDfsDepartmentCode"
                      class="dropdown-item dropdown-item-dfs_department_code"
                      href="#"
                      data-complaint-id="${row.id}"
                      data-value="${row.dfs_department_code}"
                    >
                      ${lexicon[ currentLocal ].dropdawn.dfs_department_code}
                    </a>
                    
                    <!--dfs_executor-->
                    <a
                      data-toggle="modal"
                      data-target="#modalComplaintDfsExecutor"
                      class="dropdown-item dropdown-item-dfs_executor"
                      href="#"
                      data-complaint-id="${row.id}"
                      data-value="${row.dfs_executor}"
                    >
                      ${lexicon[ currentLocal ].dropdawn.dfs_executor}
                    </a>
                    
                    <!--gotToMap-->
                    <a
                      data-toggle="modal"
                      data-target="#modalComplaintToAddressConfirm"
                      class="dropdown-item dropdown-item-complaint-to-address"
                      href="#"
                      data-complaint-id="${row.id}"
                    >
                      ${lexicon[ currentLocal ].dropdawn.gotToMap}
                    </a>
                    
                    <!--confiscated_goods-->
                    <a
                      data-toggle="modal"
                      data-target="#modalComplaintConfiscatedGoods"
                      class="dropdown-item dropdown-complaint-confiscated_goods"
                      href="#"
                      data-complaint-id="${row.id}"
                      data-value="${row.confiscated_goods}"
                    >
                      ${lexicon[ currentLocal ].dropdawn.confiscated_goods}
                    </a>
                    
                    <!--protocol_drawn_up-->
                    <a
                      data-toggle="modal"
                      data-target="#modalComplaintProtocolDrawnUp"
                      class="dropdown-item dropdown-complaint-protocol_drawn_up"
                      href="#"
                      data-complaint-id="${row.id}"
                    >
                      ${lexicon[ currentLocal ].dropdawn.protocol_drawn_up}
                    </a>
                    
                    <!--financial_sanctions-->
                    <a
                      data-toggle="modal"
                      data-target="#modalComplaintFinancialSanctions"
                      class="dropdown-item dropdown-complaint-financial_sanctions"
                      href="#"
                      data-complaint-id="${row.id}"
                    >
                      ${lexicon[ currentLocal ].dropdawn.financial_sanctions}
                    </a>
                    
                    <!--delete-->
                    <a
                      data-toggle="modal"
                      data-target="#modalComplaintDeleteConfirm"
                      class="dropdown-item dropdown-complaint-delete"
                      href="#"
                      data-complaint-id="${row.id}"
                    >
                      ${lexicon[ currentLocal ].dropdawn.delete}
                    </a>
                  </div>
                </div>
                `;
                }
            },
        ],
            language: {
            url: DataTableAPI.getLanguageUrl(),
        }
    })
    .on('init.dt',function () {
            console.log('Table initialisation complete');
            setTimeout(() => {
                $complaintListTable.draw();

            ;
        },1000)
        })
    } else {
        deleteComplaint(null);
        addressToComplaint(null);
        npCode(null);
        toCode(null);
    }

}())
