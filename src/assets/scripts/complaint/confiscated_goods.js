import * as $ from 'jquery';
import API from './api';
import {findFalse, isNumber, maxLength, required} from "../address/validation"

export default function ($table) {


  // confiscated_goods
  const $modalComplaintConfiscatedGoods = $('#modalComplaintConfiscatedGoods');

  const $modalSubmitMessage = $('#modalSubmitMessage');
  const lexicon = {

    RU: {
      button: {
        updates: 'Конфисковано товаров',
      },
      modalSubmit: {
        success: {
          title: 'Успех',
          message: (num) => `Добавлено ${num} конфискованного товара`,
        },
        error: {
          title: 'Ошибка',
          message: `Произошла непредвиденная ошибка. Попробуйте перезагрузить страницу и повторить запрос.`,
        },
      }

    },
    UK: {
      button: {
        updates: 'Конфісковано товарів',
      },
      modalSubmit: {
        success: {
          title: 'Успіх',
          message: (num) => `Додано $ {num} конфіскованого товара`,
        },
        error: {
          title: 'Помилка',
          message: `Сталася неочікувана помилка. Спробуйте перезавантажити сторінку і повторити запит.`,
        },
      }
    }
  };

  let currentLocal = ( window.config && window.config.lang) || 'RU';
  currentLocal = currentLocal.toUpperCase();

  if (currentLocal !== 'RU' || currentLocal !== 'UK') {
    currentLocal = 'RU';
  }

  $('.dropdown-complaint-confiscated_goods').unbind('click').bind('click', function () {

    const ID = $(this).attr('data-complaint-id');
    const DEFAULT_VALUE = $(this).attr('data-value');
    const $input = $modalComplaintConfiscatedGoods.find('[name="confiscated_goods"]');


    $modalComplaintConfiscatedGoods.find('.btn-add').bind('click', function () {

      const VALUE = $input.val();
      let errors = [];
      let formValidStatus = [];

      // Валидация
      if (required(VALUE)) {
        errors.push(required(VALUE));
        formValidStatus.push(false);
      }
      if (isNumber(VALUE)) {
        errors.push(isNumber(VALUE));
        formValidStatus.push(false);
      }
      if (maxLength(255)(VALUE)) {
        errors.push(maxLength(255)(VALUE));
        formValidStatus.push(false);
      }


      if (!findFalse(formValidStatus, false)) {
        const $invalid_feedback = $input.next(".invalid-feedback");
        if ($invalid_feedback) {
          // errors
          $invalid_feedback.empty();
          let msg_nodes = '';
          errors.map((msg) => {
            msg_nodes += `<div>${msg}</div>`;
          });
          $invalid_feedback.append(msg_nodes);
          $input.addClass('is-invalid');
        }

      } else {
        //reset
        $input.removeClass('is-invalid');
        $input.val('');
        $modalComplaintConfiscatedGoods.modal('hide');
        $modalSubmitMessage.modal('hide');
        $modalComplaintConfiscatedGoods.find('.btn-add').unbind('click');


        API.actionTaken(ID, {confiscated_goods: VALUE})
          .then((response) => {
            console.log(response);
            if ($table) {
              $table.ajax.reload(function (data) {
                console.log('RELOAD TABLES!', data)
              }, false);
            }
            $modalSubmitMessage.find('#modalSubmitMessageLabel').html(lexicon[currentLocal].modalSubmit.success.title);
            $modalSubmitMessage.find('.data-message-value').html(lexicon[currentLocal].modalSubmit.success.message(VALUE));
            $modalSubmitMessage.modal('show');
          })
          .catch(err => {
            console.error(err);

            $modalSubmitMessage.find('#modalSubmitMessageLabel').html(lexicon[currentLocal].modalSubmit.error.title);
            $modalSubmitMessage.find('.data-message-value').html(lexicon[currentLocal].modalSubmit.error.message.message(VALUE));
            $modalSubmitMessage.modal('show');
          })
      }


    })

  })

}
