import API from './api';
import * as $ from 'jquery';


export default function ($table) {

  const $modalComplaintDeleteConfirm = $('#modalComplaintDeleteConfirm');
  const $modalSubmitMessage = $('#modalSubmitMessage');

  const lexicon = {

    RU: {
      button: {
        updates: 'Издать обновление',
      },
      modalSubmit: {
        success: {
          title: 'Успех',
          message: 'Жалоба удалена.',
        },
        error: {
          title: 'Ошибка',
          message: `Произошла непредвиденная ошибка. Попробуйте перезагрузить страницу и повторить запрос.`,
        },
      }

    },
    UK: {
      button: {
        updates: 'Видати оновлення',
      },
      modalSubmit: {
        success: {
          title: 'Успіх',
          message: 'Скарга не очищено.',
        },
        error: {
          title: 'Помилка',
          message: `Сталася неочікувана помилка. Спробуйте перезавантажити сторінку і повторити запит.`,
        },
      }
    }
  };

  let currentLocal = ( window.config && window.config.lang) || 'RU';
  currentLocal = currentLocal.toUpperCase();

  if (currentLocal !== 'RU' || currentLocal !== 'UK') {
    currentLocal = 'RU';
  }

  $('.dropdown-complaint-delete').unbind('click').bind('click', function (e) {
    e.preventDefault();
    const ID = $(this).attr('data-complaint-id');
    console.log('ID', ID);

    $modalComplaintDeleteConfirm.find('.btn-delete').on('click', e => {

      $modalComplaintDeleteConfirm.modal('hide');

      // $modalPreLoader.modal('show');
      $modalComplaintDeleteConfirm.find('.btn-delete').unbind('click');
      console.log(ID);
      API.deleteComplaint(ID)
        .then((response) => {
            if($table){
                $table.ajax.reload(function (data) {
                    console.log('RELOAD TABLES!',data)
                }, false);
            }
          $modalSubmitMessage.find('#modalSubmitMessageLabel').html(lexicon[currentLocal].modalSubmit.success.title);
          $modalSubmitMessage.find('.data-message-value').html(lexicon[currentLocal].modalSubmit.success.message);
          $modalSubmitMessage.modal('show');

          console.log(response);
        })
        .catch(err => {
          // $modalPreLoader.modal('hide');
          $modalSubmitMessage.find('#modalSubmitMessageLabel').html(lexicon[currentLocal].modalSubmit.error.title);
          $modalSubmitMessage.find('.data-message-value').html(lexicon[currentLocal].modalSubmit.error.message);
          $modalSubmitMessage.modal('show');
          console.error(err);
        })
    });

  });
}
