import * as $ from 'jquery';
import {fetchAPI} from '../utils/fetch';

export default {

  addNpCode(id, code) {
    console.log(id);
    return new Promise((resolve, reject) => {
      fetchAPI(`/complaint/${id}`, 'PUT', {np_code: code})
        .then((response) => {
          return response.json()
        })
        .then((response) => {
          resolve(response);
        })
        .catch((err) => {
          reject(err);
        });
    })
  },
  addToCode(id, code) {
    console.log(id);
    return new Promise((resolve, reject) => {
      fetchAPI(`/complaint/${id}`, 'PUT', {to_code: code})
        .then((response) => {
          return response.json()
        })
        .then((response) => {
          resolve(response);
        })
        .catch((err) => {
          reject(err);
        });

    })
  },
  deleteComplaint(id) {
    console.log(id);
    return new Promise((resolve, reject) => {
      fetchAPI(`/complaint/${id}`, 'DELETE')
        .then((response) => {
          return response.json()
        })
        .then((response) => {
          resolve(response);
        })
        .catch((err) => {
          reject(err);
        });
    })
  },
  complaintToAddress(id) {
    console.log(id);
    return new Promise((resolve, reject) => {
      fetchAPI(`/complaint/transform/${id}`)
        .then((response) => {
          return response.json()
        })
        .then((response) => {
          resolve(response);
        })
        .catch((err) => {
          reject(err);
        });
    });
  },
  changeStatus(id, state) {
    console.log(id);
    return new Promise((resolve, reject) => {
      fetchAPI(`/complaint/${id}`, 'PUT', {status: state})
        .then((response) => {
          return response.json()
        })
        .then((response) => {
          resolve(response);
        })
        .catch((err) => {
          reject(err);
        });
    })
  },
  dfsDepartmentCode(id, data) {
    console.log(id);
    return new Promise((resolve, reject) => {
      fetchAPI(`/complaint/${id}`, 'PUT', {dfs_department_code: data})
        .then((response) => {
          return response.json()
        })
        .then((response) => {
          resolve(response);
        })
        .catch((err) => {
          reject(err);
        });
    })
  },
  dfsExecutor(id, data) {
    console.log(id);
    return new Promise((resolve, reject) => {
      fetchAPI(`/complaint/${id}`, 'PUT', {dfs_executor: data})
        .then((response) => {
          return response.json()
        })
        .then((response) => {
          resolve(response);
        })
        .catch((err) => {
          reject(err);
        });
    })
  },
  actionTaken(id, data) {
    console.log(id);
    return new Promise((resolve, reject) => {
      fetchAPI(`/complaint/${id}/action_taken`, 'POST', data)
        .then((response) => {
          return response.json()
        })
        .then((response) => {
          resolve(response);
        })
        .catch((err) => {
          reject(err);
        });
    })
  },
  getViolations() {

    return new Promise((resolve, reject) => {
      fetchAPI("/violations", 'GET')
        .then((response) => {
          console.log('getViolations: ',response);
          return response.json();
        })
        .then((response) => {
          resolve(response);
        })
        .catch((err) => {
          reject(err);
        });
    });
  },

  getAddresses() {

    return new Promise((resolve, reject) => {
      fetchAPI("http://185.25.117.8/map", 'GET')
        .then((response) => {
          console.log('getViolations: ',response);
          return response.json();
        })
        .then((response) => {
          resolve(response);
        })
        .catch((err) => {
          reject(err);
        });
    });
  },

  updateComplaint(COMPLAINT_ID, data) {

    return new Promise((resolve, reject) => {
      fetchAPI(`/complaint/${COMPLAINT_ID}`, 'PUT',data)
        .then((response) => {
          console.log('getViolations: ',response);
          return response.json();
        })
        .then((response) => {
          resolve(response);
        })
        .catch((err) => {
          reject(err);
        });
    });
  },


};
