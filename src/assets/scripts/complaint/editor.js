import * as $ from 'jquery';
import API from './api';
import Vue from 'vue'
import Multiselect from 'vue-multiselect'
import 'vue-multiselect/dist/vue-multiselect.min.css'
import {ValidationForm} from "../address/validation";
import {formConfig} from "./formConfig";
//https://vue-multiselect.js.org/
const createViolationsList = ({data}) => {
  const $violationsList = $('#violationsList');

  if (data) {
    const elemList = data.map((item,index) => {
      return `<li class="form-check">
                    <label class="form-check-label" for="Violation-${item.id}" title="${item.description}">
                      <input 
                        id="Violation-${item.id}" 
                        class="form-check-input" 
                        type="checkbox" 
                        name="violations" 
                        value="${item.name}"
                      > ${item.name}
                    </label>
                  </li>`
    });

    $violationsList.append(elemList);
  }


};


const createAddressSelect = (data) => {

  const $formComplaintEdit = $('#formComplaintEdit');

  const options = data.map((item) => ({
    address_id: item.id,
    address: item.address,
    lng: item.lng,
    lat: item.lat,
  }));


  new Vue({
    components: {
      Multiselect
    },
    data() {
      return {
        value: {
          address_id: $formComplaintEdit.find(`[name="address_id"]`).val(),
          address: $formComplaintEdit.find(`[name="address"]`).val(),
          lng: $formComplaintEdit.find(`[name="lng"]`).val(),
          lat: $formComplaintEdit.find(`[name="lat"]`).val(),
        },
        options: options
      }
    },
    methods: {
      onSelect(option) {
        console.log('onSelect - option: ',option);
        $formComplaintEdit.find(`[name="address_id"]`).val(option.address_id);
        $formComplaintEdit.find(`[name="address"]`).val(option.address);
        $formComplaintEdit.find(`[name="lng"]`).val(option.lng);
        $formComplaintEdit.find(`[name="lat"]`).val(option.lat);


      },
    }
  }).$mount('#addressSelect')


};


// TODO: склеить нарешения в одну строку (добавить проверку на активность маркера)
// TODO: добавить в селект адресов значение по умолчанию

const submitComplaints = (event) => {
  const value = {};

  const $modalSubmitMessage = $('#modalSubmitMessage');
  const $formComplaintEdit = $('#formComplaintEdit');
  const COMPLAINT_ID = $(event.target).attr('data-complaint-id');
  const REDIRECT = $(event.target).attr('data-redirect');
  event.preventDefault();

  console.log('events: ',event);
  console.log('value: ',value);
  console.log('COMPLAINT_ID: ',COMPLAINT_ID);
  console.log('REDIRECT: ',REDIRECT);

  const lexicon = {

    RU: {
      modalSubmit: {
        success: {
          title: 'Успех',
          message: `Жалоба обновлена.`,
        },
        error: {
          title: 'Ошибка',
          message: `Произошла непредвиденная ошибка. Попробуйте перезагрузить страницу и повторить запрос.`,
        },
      }

    },
    UK: {
      modalSubmit: {
        success: {
          title: 'Успіх',
          message: `Жалоба обновлена.`,
        },
        error: {
          title: 'Помилка',
          message: `Сталася неочікувана помилка. Спробуйте перезавантажити сторінку і повторити запит.`,
        },
      }
    }
  };

  let currentLocal = ( window.config && window.config.lang) || 'RU';
  currentLocal = currentLocal.toUpperCase();

  if (currentLocal !== 'RU' || currentLocal !== 'UK') {
    currentLocal = 'RU';
  }

  if (ValidationForm($formComplaintEdit,formConfig)) {
    formConfig.map((item) => {
      if (item.name === 'violations') {
        console.log($formComplaintEdit.find(`[name="${item.name}"]`));
        const violations = $formComplaintEdit.find(`[name="${item.name}"]`);
        const length = violations.length;
        const valArray = [];
        for (let i = 0 ; i < length ; i += 1) {

          if ($(violations[ i ]).prop('checked')) {
            valArray.push($(violations[ i ]).val());
          }
        }
        if (valArray.length) {
          value[ item.name ] = valArray.join(';')
        }


      } else {
        let val = $($formComplaintEdit.find(`[name="${item.name}"]`)[ 0 ]).val();

        if (val) {
          value[ item.name ] = val
        }
      }


    });
    console.log('value: ',value);


    API.updateComplaint(COMPLAINT_ID,value)
      .then((response) => {
        console.log(response);
        $modalSubmitMessage.find('#modalSubmitMessageLabel').html(lexicon[ currentLocal ].modalSubmit.success.title);
        $modalSubmitMessage.find('.data-message-value').html(lexicon[ currentLocal ].modalSubmit.success.message);
        $modalSubmitMessage.modal('show');
        if (REDIRECT) {
          // при успешно созданом адресе редеректить нужно в любом случае на другую страницу
          setTimeout(() => window.location.replace(REDIRECT),3000);
        }
      })
      .catch((error) => {
        console.error(error);
        $modalSubmitMessage.find('#modalSubmitMessageLabel').html(lexicon[ currentLocal ].modalSubmit.error.title);
        $modalSubmitMessage.find('.data-message-value').html(lexicon[ currentLocal ].modalSubmit.error.message);
        $modalSubmitMessage.modal('show');
      });
  }

};

export default (function () {
  const $formComplaintEdit = $('#formComplaintEdit');
  if ($formComplaintEdit) {

    // API.getViolations()
    //   .then((response) => {
    //     console.log(response);
    //     createViolationsList(response)
    //   })
    //   .catch((error) => {
    //     console.error(error);
    //   });

    API.getAddresses()
      .then((response) => {
        console.log(response);
        createAddressSelect(response);
      })
      .catch((error) => {
        console.error(error);
      });

    $('.complaintSubmit').on('click',submitComplaints)

  }

}())
