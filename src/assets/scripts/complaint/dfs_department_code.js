import * as $ from 'jquery';
import API from './api';
import {findFalse, isNumber, maxLength, required} from "../address/validation"
export default function ($table) {

  const $modalAddNpCode = $('#modalComplaintDfsDepartmentCode');

  const $modalSubmitMessage = $('#modalSubmitMessage');
  const lexicon = {

    RU: {
      button: {
        updates: 'Издать обновление',
      },
      modalSubmit: {
        success: {
          title: 'Успех',
          message: 'Код отдела ДФС успешно обновлен.',
        },
        error: {
          title: 'Ошибка',
          message: `Произошла непредвиденная ошибка. Попробуйте перезагрузить страницу и повторить запрос.`,
        },
      }

    },
    UK: {
      button: {
        updates: 'Видати оновлення',
      },
      modalSubmit: {
        success: {
          title: 'Успіх',
          message: 'Код відділу ДФС успішно оновлено.',
        },
        error: {
          title: 'Помилка',
          message: `Сталася неочікувана помилка. Спробуйте перезавантажити сторінку і повторити запит.`,
        },
      }
    }
  };

  let currentLocal = ( window.config && window.config.lang) || 'RU';
  currentLocal = currentLocal.toUpperCase();

  if (currentLocal !== 'RU' || currentLocal !== 'UK') {
    currentLocal = 'RU';
  }
  $('.dropdown-item-dfs_department_code').unbind('click').bind('click', function (e) {
    e.preventDefault();
    const ID = $(this).attr('data-complaint-id');
    const DEFAULT_VALUE = $(this).attr('data-value');
    const $input = $modalAddNpCode.find('[name="dfs_department_code"]');
    $input.val(DEFAULT_VALUE);
    console.log('ID',ID);

    $modalAddNpCode.find('.btn-add').bind('click', e => {

      let VALUE = $input.val();
      let errors = [];
      let formValidStatus = [];
      if(required(VALUE)) {
        errors.push(required(VALUE));
        formValidStatus.push(false);
      }
      if(maxLength(255)(VALUE)) {
        errors.push(maxLength(255)(VALUE));
        formValidStatus.push(false);
      }

      if(!findFalse(formValidStatus, false)) {
        const $invalid_feedback = $input.next(".invalid-feedback");
        if ($invalid_feedback) {
          // errors
          $invalid_feedback.empty();
          let msg_nodes = '';
          errors.map((msg) => {
            msg_nodes += `<div>${msg}</div>`;
          });
          $invalid_feedback.append(msg_nodes);
          $input.addClass('is-invalid');
        }
      } else {
        $modalAddNpCode.modal('hide');
        $modalSubmitMessage.modal('hide');
        $input.val('');
        $modalAddNpCode.find('.btn-add').unbind('click');

        API.dfsDepartmentCode(ID, VALUE)
          .then((response) => {
            console.log(response);
              if($table){
                  $table.ajax.reload(function (data) {
                      console.log('RELOAD TABLES!',data)
                  }, false);
              }
            $modalSubmitMessage.find('#modalSubmitMessageLabel').html(lexicon[currentLocal].modalSubmit.success.title);
            $modalSubmitMessage.find('.data-message-value').html(lexicon[currentLocal].modalSubmit.success.message);
            $modalSubmitMessage.modal('show');
          })
          .catch(err => {
            console.error(err);

            $modalSubmitMessage.find('#modalSubmitMessageLabel').html(lexicon[currentLocal].modalSubmit.error.title);
            $modalSubmitMessage.find('.data-message-value').html(lexicon[currentLocal].modalSubmit.error.message);
            $modalSubmitMessage.modal('show');
          })
      }
    })
  })

}
