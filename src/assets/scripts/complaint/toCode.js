import * as $ from 'jquery';
import API from './api';
import {findFalse, isNumber, maxLength, required} from "../address/validation"

export default function ($table) {

    const $modalAddToCode = $('#modalAddToCode');
    const $inputToCodeText = $('#ToCodeText');

    const $modalSubmitMessage = $('#modalSubmitMessage');
    const lexicon = {
        RU: {
            button: {
                updates: 'Издать обновление',
            },
            modalSubmit: {
                success: {
                    title: 'Успех',
                    message: (TO_CODE) => `ТО код <b>“${TO_CODE}”</b> успешно добавлен`,
                },
                error: {
                    title: 'Ошибка',
                    message: `Произошла непредвиденная ошибка. Попробуйте перезагрузить страницу и повторить запрос.`,
                },
            }

        },
        UK: {
            button: {
                updates: 'Видати оновлення',
            },
            modalSubmit: {
                success: {
                    title: 'Успіх',
                    message: (TO_CODE) => `ТО код <b>“${TO_CODE}”</b> доданий`,
                },
                error: {
                    title: 'Помилка',
                    message: `Сталася неочікувана помилка. Спробуйте перезавантажити сторінку і повторити запит.`,
                },
            }
        }
    };

    let currentLocal = ( window.config && window.config.lang) || 'RU';
    currentLocal = currentLocal.toUpperCase();

    if (currentLocal !== 'RU' || currentLocal !== 'UK') {
        currentLocal = 'RU';
    }

    $('.dropdown-item-add-to-code').unbind('click').bind('click', function (e) {
        e.preventDefault();
        const ID = $(this).attr('data-complaint-id');
        console.log('ID', ID);

        $modalAddToCode.find('.btn-add').on('click', e => {
            const TO_CODE = $inputToCodeText.val();
            const errors = [];
            const formValidStatus = [];
            console.log(TO_CODE);
            if (required(TO_CODE)) {
                errors.push(required(TO_CODE));
                formValidStatus.push(false);
            }
            if (isNumber(TO_CODE)) {
                errors.push(isNumber(TO_CODE));
                formValidStatus.push(false);
            }
            if (maxLength(255)(TO_CODE)) {
                errors.push(maxLength(255)(TO_CODE));
                formValidStatus.push(false);
            }

            if (!findFalse(formValidStatus, false)) {
                const $invalid_feedback = $inputToCodeText.next(".invalid-feedback");
                if ($invalid_feedback) {
                    // errors
                    $invalid_feedback.empty();
                    let msg_nodes = '';
                    errors.map((msg) => {
                        msg_nodes += `<div>${msg}</div>`;
                    });
                    $invalid_feedback.append(msg_nodes);
                    $inputToCodeText.addClass('is-invalid');
                }
            } else {
                $modalAddToCode.modal('hide');
                $modalSubmitMessage.modal('hide');
                // $modalPreLoader.modal('show');
                $inputToCodeText.val('');
                $modalAddToCode.find('.btn-add').unbind('click');
                API.addToCode(ID, TO_CODE)
                    .then((response) => {
                        console.log(response);

                        if ($table) {
                            $table.ajax.reload(function (data) {
                                console.log('RELOAD TABLES!', data)
                            }, false);
                        }

                        $modalSubmitMessage.find('#modalSubmitMessageLabel').html(lexicon[currentLocal].modalSubmit.success.title);
                        $modalSubmitMessage.find('.data-message-value').html(lexicon[currentLocal].modalSubmit.success.message(TO_CODE));
                        $modalSubmitMessage.modal('show');
                    })
                    .catch(err => {
                        console.error(err);
                        $modalSubmitMessage.find('#modalSubmitMessageLabel').html(lexicon[currentLocal].modalSubmit.error.title);
                        $modalSubmitMessage.find('.data-message-value').html(lexicon[currentLocal].modalSubmit.error.message);
                        $modalSubmitMessage.modal('show');
                    })
            }
        })
    })

}
