import * as $ from 'jquery';
import API from './api';
import {findFalse, isNumber, maxLength, required} from "../address/validation"
export default function ($table) {
  console.log('NP CODE HANDLER');
  const $modalAddNpCode = $('#modalAddNpCode');

  const $modalSubmitMessage = $('#modalSubmitMessage');
  const lexicon = {

    RU: {
      button: {
        updates: 'Издать обновление',
      },
      modalSubmit: {
        success: {
          title: 'Успех',
          message: (NP_CODE) => `НП код <b>“${NP_CODE}”</b> успешно добавлен`,
        },
        error: {
          title: 'Ошибка',
          message: `Произошла непредвиденная ошибка. Попробуйте перезагрузить страницу и повторить запрос.`,
        },
      }

    },
    UK: {
      button: {
        updates: 'Видати оновлення',
      },
      modalSubmit: {
        success: {
          title: 'Успіх',
          message: (NP_CODE) => `НП код <b>“${NP_CODE}”</b> доданий`,
        },
        error: {
          title: 'Помилка',
          message: `Сталася неочікувана помилка. Спробуйте перезавантажити сторінку і повторити запит.`,
        },
      }
    }
  };

  let currentLocal = ( window.config && window.config.lang) || 'RU';
  currentLocal = currentLocal.toUpperCase();

  if (currentLocal !== 'RU' || currentLocal !== 'UK') {
    currentLocal = 'RU';
  }
  $('.dropdown-item-add-np-code').unbind('click').bind('click', function (e) {
    e.preventDefault();
    const ID = $(this).attr('data-complaint-id');
    console.log('ID',ID);
    $modalAddNpCode.find('.btn-add').bind('click', e => {
      // const $inputNpCodeText = $('#NpCodeText');
      let $inputNpCodeText = $modalAddNpCode.find('#NpCodeText');
      let NP_CODE = $inputNpCodeText.val();
      let errors = [];
      let formValidStatus = [];
      if(required(NP_CODE)) {
        errors.push(required(NP_CODE));
        formValidStatus.push(false);
      }
      if(isNumber(NP_CODE)) {
        errors.push(isNumber(NP_CODE));
        formValidStatus.push(false);
      }
      if(maxLength(255)(NP_CODE)) {
        errors.push(maxLength(255)(NP_CODE));
        formValidStatus.push(false);
      }

      if(!findFalse(formValidStatus, false)) {
        const $invalid_feedback = $inputNpCodeText.next(".invalid-feedback");
        if ($invalid_feedback) {
          // errors
          $invalid_feedback.empty();
          let msg_nodes = '';
          errors.map((msg) => {
            msg_nodes += `<div>${msg}</div>`;
          });
          $invalid_feedback.append(msg_nodes);
          $inputNpCodeText.addClass('is-invalid');
        }
      } else {
        $modalAddNpCode.modal('hide');
        $modalSubmitMessage.modal('hide');
        $inputNpCodeText.val('');
        $modalAddNpCode.find('.btn-add').unbind('click');
        API.addNpCode(ID, NP_CODE)
          .then((response) => {
            console.log(response);
              if($table){
                  $table.ajax.reload(function (data) {
                      console.log('RELOAD TABLES!',data)
                  }, false);
              }
            $modalSubmitMessage.find('#modalSubmitMessageLabel').html(lexicon[currentLocal].modalSubmit.success.title);
            $modalSubmitMessage.find('.data-message-value').html(lexicon[currentLocal].modalSubmit.success.message(NP_CODE));
            $modalSubmitMessage.modal('show');
          })
          .catch(err => {
            console.error(err);

            $modalSubmitMessage.find('#modalSubmitMessageLabel').html(lexicon[currentLocal].modalSubmit.error.title);
            $modalSubmitMessage.find('.data-message-value').html(lexicon[currentLocal].modalSubmit.error.message);
            $modalSubmitMessage.modal('show');
          })
      }
    })
  })

}
