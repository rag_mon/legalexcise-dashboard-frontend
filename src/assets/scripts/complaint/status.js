import * as $ from 'jquery';
import API from './api';

export default function () {
  let currentLocal = ( window.config && window.config.lang) || 'RU';
  currentLocal = currentLocal.toUpperCase();

  if (currentLocal !== 'RU' || currentLocal !== 'UK') {
    currentLocal = 'RU';
  }
  const $modalSubmitMessage = $('#modalSubmitMessage');
  const $table = $('#complaintListTable');
  const lexicon = {
    RU: {
      modalSubmit: {
        success: {
          title: 'Успех',
          message: 'Статус изменен',
        },
        error: {
          title: 'Ошибка',
          message: `Произошла непредвиденная ошибка. Попробуйте перезагрузить страницу и повторить запрос.`,
        },
      }

    },
    UK: {
      modalSubmit: {
        success: {
          title: 'Успіх',
          message: 'Статус змінений',
        },
        error: {
          title: 'Помилка',
          message: `Сталася неочікувана помилка. Спробуйте перезавантажити сторінку і повторити запит.`,
        },
      }
    }
  };


  $table.find(`select[name="status"]`).on('change', function (event) {
    console.log(event);
    const ID = $(this).attr('data-complaint-id');
    const VALUE = $(this).val() != 'null' ? $(this).val() : null;
    console.log('ID',ID);
    console.log('VALUE',VALUE);
    console.log('is real null value', VALUE === null);


    API.changeStatus(ID, VALUE)
      .then((response) => {
        console.log(response);
        $modalSubmitMessage.find('#modalSubmitMessageLabel').html(lexicon[currentLocal].modalSubmit.success.title);
        $modalSubmitMessage.find('.data-message-value').html(lexicon[currentLocal].modalSubmit.success.message);
        $modalSubmitMessage.modal('show');
      })
      .catch(err => {
        console.error(err);
        $modalSubmitMessage.find('#modalSubmitMessageLabel').html(lexicon[currentLocal].modalSubmit.error.title);
        $modalSubmitMessage.find('.data-message-value').html(lexicon[currentLocal].modalSubmit.error.message);
        $modalSubmitMessage.modal('show');
      })

  })

}
