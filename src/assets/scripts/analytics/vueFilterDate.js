import Vue from 'vue'
import * as $ from "jquery";

// You need a specific loader for CSS files
import 'vue-datetime/dist/vue-datetime.css'
import {DateTime as LuxonDateTime} from 'luxon'
import {Datetime} from 'vue-datetime';
import API from './api';
import {AreaChart, BarChart, EasyPieChart, LineChart, ScatterChart} from './graphicsTypes/index'


export const FILTER_INIT = (date) => {
  const initChart = (chart) => {
    switch (chart.type) {
      case('area'): {
        AreaChart(chart);
        break
      }
      case('bar'): {
        BarChart(chart);
        break
      }
      case('easy_pie'): {
        EasyPieChart(chart);
        break
      }
      case('line'): {
        LineChart(chart);
        break
      }
      case('scatter'): {
        ScatterChart(chart);
        break
      }
    }
  };
  Vue.component('datetime', Datetime);
  window.LuxonDateTime = LuxonDateTime;

  if(!$(date.filter).length) return null
  const FilterOfChart = new Vue({
    el: date.filter,
    template: `
      <div class="row">
        <div class="col-md-6 form-group">
          <datetime type="datetime" :disabled="disabled" v-model="filter_start_at"></datetime>
        </div>
        <div class="col-md-6 form-group">
          <datetime type="datetime" :disabled="disabled" v-model="filter_end_at"></datetime>
        </div>
      </div>
    `,
    data: {
      filter_start_at: null,
      filter_end_at: null,
      chartType: date.type,
      chartProvider: date.filterProvider,
      disabled: false,
    },
    watch: {
      filter_start_at: function (value) {
        if (value) {
          this.updateChart(LuxonDateTime.fromISO(value).toFormat('dd.MM.yyyy HH:mm:ss'), 'filter_start_at');
        }
      },
      filter_end_at: function (value) {
        if (value) {
          this.updateChart(LuxonDateTime.fromISO(value).toFormat('dd.MM.yyyy HH:mm:ss'), 'filter_end_at');
        }
      },
    },
    methods: {
      updateChart: function (value, type) {
        this.disabled = true;
        console.log('updateChart: ', value, type);
        console.log('chartProvider:', this.chartProvider);
        console.log('chartType:', this.chartType);
        API.dateFilter(this.chartProvider, {[type]: value})
          .then((response) => {
            initChart(response);
            this.disabled = false;
          })
          .catch((error) => {
            this.disabled = false;
            console.error(error);
          });
      }
    }
  });






}


















// export default (function () {
//
//   Vue.component('datetime', Datetime);
//   window.LuxonDateTime = LuxonDateTime;
//
//   const FilterOfChartArea = new Vue({
//     el: '#filter-of-chart-bar',
//     template: `
//       <div class="row">
//         <div class="col-md-6 form-group">
//           <datetime type="datetime" :disabled="disabled" v-model="filter_start_at"></datetime>
//         </div>
//         <div class="col-md-6 form-group">
//           <datetime type="datetime" :disabled="disabled" v-model="filter_end_at"></datetime>
//         </div>
//       </div>
//     `,
//     data: {
//       filter_start_at: null,
//       filter_end_at: null,
//       chartType: 'bar',
//       chartProvider: 'adding_addresses',
//       disabled: false,
//     },
//     watch: {
//       filter_start_at: function (value) {
//         if (value) {
//           this.updateChart(LuxonDateTime.fromISO(value).toFormat('dd.MM.yyyy HH:mm:ss'), 'filter_start_at');
//         }
//       },
//       filter_end_at: function (value) {
//         if (value) {
//           this.updateChart(LuxonDateTime.fromISO(value).toFormat('dd.MM.yyyy HH:mm:ss'), 'filter_end_at');
//         }
//       },
//     },
//     methods: {
//       updateChart: function (value, type) {
//         this.disabled = true;
//         console.log('updateChart: ', value, type);
//         console.log('chartProvider:', this.chartProvider);
//         console.log('chartType:', this.chartType);
//         API.dateFilter(this.chartProvider, {[type]: value})
//           .then((response) => {
//             initChart(response);
//             this.disabled = false;
//           })
//           .catch((error) => {
//             this.disabled = false;
//             console.error(error);
//           });
//       }
//     }
//   });
//
//
//
//
//   //
//   // BAR-CHART DATE FILTER
//   //
//   // const FilterOfChartArea = new Vue({
//   //   el: '#filter-of-chart-bar',
//   //   template: `
//   //     <div class="row">
//   //       <div class="col-md-6 form-group">
//   //         <datetime type="datetime" :disabled="disabled" v-model="filter_start_at"></datetime>
//   //       </div>
//   //       <div class="col-md-6 form-group">
//   //         <datetime type="datetime" :disabled="disabled" v-model="filter_end_at"></datetime>
//   //       </div>
//   //     </div>
//   //   `,
//   //   data: {
//   //     filter_start_at: null,
//   //     filter_end_at: null,
//   //     chartType: 'bar',
//   //     chartProvider: 'adding_addresses',
//   //     disabled: false,
//   //   },
//   //   watch: {
//   //     filter_start_at: function (value) {
//   //       if (value) {
//   //         this.updateChart(LuxonDateTime.fromISO(value).toFormat('dd.MM.yyyy HH:mm:ss'), 'filter_start_at');
//   //       }
//   //     },
//   //     filter_end_at: function (value) {
//   //       if (value) {
//   //         this.updateChart(LuxonDateTime.fromISO(value).toFormat('dd.MM.yyyy HH:mm:ss'), 'filter_end_at');
//   //       }
//   //     },
//   //   },
//   //   methods: {
//   //     updateChart: function (value, type) {
//   //       this.disabled = true;
//   //       console.log('updateChart: ', value, type);
//   //       console.log('chartProvider:', this.chartProvider);
//   //       console.log('chartType:', this.chartType);
//   //       API.dateFilter(this.chartProvider, {[type]: value})
//   //         .then((response) => {
//   //           initChart(response);
//   //           this.disabled = false;
//   //         })
//   //         .catch((error) => {
//   //           this.disabled = false;
//   //           console.error(error);
//   //         });
//   //     }
//   //   }
//   // });
//
//   // //
//   // // BAR-CHART DATE FILTER
//   // //
//   // const FilterOfChartBar = new Vue({
//   //   el: '#filter-of-chart-bar',
//   //   template: `
//   //     <div class="row">
//   //       <div class="col-md-6 form-group">
//   //         <datetime type="datetime" :disabled="disabled" v-model="filter_start_at"></datetime>
//   //       </div>
//   //       <div class="col-md-6 form-group">
//   //         <datetime type="datetime" :disabled="disabled" v-model="filter_end_at"></datetime>
//   //       </div>
//   //     </div>
//   //   `,
//   //   data: {
//   //     filter_start_at: null,
//   //     filter_end_at: null,
//   //     chartType: 'bar',
//   //     chartProvider: 'adding_addresses',
//   //     disabled: false,
//   //   },
//   //   watch: {
//   //     filter_start_at: function (value) {
//   //       if (value) {
//   //         this.updateChart(LuxonDateTime.fromISO(value).toFormat('dd.MM.yyyy HH:mm:ss'), 'filter_start_at');
//   //       }
//   //     },
//   //     filter_end_at: function (value) {
//   //       if (value) {
//   //         this.updateChart(LuxonDateTime.fromISO(value).toFormat('dd.MM.yyyy HH:mm:ss'), 'filter_end_at');
//   //       }
//   //     },
//   //   },
//   //   methods: {
//   //     updateChart: function (value, type) {
//   //       this.disabled = true;
//   //       console.log('updateChart: ', value, type);
//   //       console.log('chartProvider:', this.chartProvider);
//   //       console.log('chartType:', this.chartType);
//   //       API.dateFilter(this.chartProvider, {[type]: value})
//   //         .then((response) => {
//   //           initChart(response);
//   //           this.disabled = false;
//   //         })
//   //         .catch((error) => {
//   //           this.disabled = false;
//   //           console.error(error);
//   //         });
//   //     }
//   //   }
//   // });
//   //
//   //
//   //
//   // //
//   // // LINE-CHART DATE FILTER
//   // //
//   // const FilterOfChartLine = new Vue({
//   //   el: '#filter-of-chart-line',
//   //   template: `
//   //     <div class="row">
//   //       <div class="col-md-6 form-group">
//   //         <datetime type="datetime" :disabled="disabled" v-model="filter_start_at"></datetime>
//   //       </div>
//   //       <div class="col-md-6 form-group">
//   //         <datetime type="datetime" :disabled="disabled" v-model="filter_end_at"></datetime>
//   //       </div>
//   //     </div>
//   //   `,
//   //   data: {
//   //     filter_start_at: null,
//   //     filter_end_at: null,
//   //     chartType: 'line',
//   //     chartProvider: 'complaint_reactions',
//   //     disabled: false,
//   //   },
//   //   watch: {
//   //     filter_start_at: function (value) {
//   //       if (value) {
//   //         this.updateChart(LuxonDateTime.fromISO(value).toFormat('dd.MM.yyyy HH:mm:ss'), 'filter_start_at');
//   //       }
//   //     },
//   //     filter_end_at: function (value) {
//   //       if (value) {
//   //         this.updateChart(LuxonDateTime.fromISO(value).toFormat('dd.MM.yyyy HH:mm:ss'), 'filter_end_at');
//   //       }
//   //     },
//   //   },
//   //   methods: {
//   //     updateChart: function (value, type) {
//   //       this.disabled = true;
//   //       console.log('updateChart: ', value, type);
//   //       console.log('chartProvider:', this.chartProvider);
//   //       console.log('chartType:', this.chartType);
//   //       API.dateFilter(this.chartProvider, {[type]: value})
//   //         .then((response) => {
//   //           init(response);
//   //           this.disabled = false;
//   //         })
//   //         .catch((error) => {
//   //           this.disabled = false;
//   //           console.error(error);
//   //         });
//   //     }
//   //   }
//   // });
//   //
//   //
//   // //
//   // // EASY PIE CHARTS DATE FILTER
//   // //
//   // const FilterOfChartEasyPie = new Vue({
//   //   el: '#filter-of-chart-line',
//   //   template: `
//   //     <div class="row">
//   //       <div class="col-md-6 form-group">
//   //         <datetime type="datetime" :disabled="disabled" v-model="filter_start_at"></datetime>
//   //       </div>
//   //       <div class="col-md-6 form-group">
//   //         <datetime type="datetime" :disabled="disabled" v-model="filter_end_at"></datetime>
//   //       </div>
//   //     </div>
//   //   `,
//   //   data: {
//   //     filter_start_at: null,
//   //     filter_end_at: null,
//   //     chartType: 'spark',
//   //     chartProvider: 'suspicious_to_active_addresses',
//   //     disabled: false,
//   //   },
//   //   watch: {
//   //     filter_start_at: function (value) {
//   //       if (value) {
//   //         this.updateChart(LuxonDateTime.fromISO(value).toFormat('dd.MM.yyyy HH:mm:ss'), 'filter_start_at');
//   //       }
//   //     },
//   //     filter_end_at: function (value) {
//   //       if (value) {
//   //         this.updateChart(LuxonDateTime.fromISO(value).toFormat('dd.MM.yyyy HH:mm:ss'), 'filter_end_at');
//   //       }
//   //     },
//   //   },
//   //   methods: {
//   //     updateChart: function (value, type) {
//   //       this.disabled = true;
//   //       console.log('updateChart: ', value, type);
//   //       console.log('chartProvider:', this.chartProvider);
//   //       console.log('chartType:', this.chartType);
//   //       API.dateFilter(this.chartProvider, {[type]: value})
//   //         .then((response) => {
//   //           init(response);
//   //           this.disabled = false;
//   //         })
//   //         .catch((error) => {
//   //           this.disabled = false;
//   //           console.error(error);
//   //         });
//   //     }
//   //   }
//   // });
//   //
//   // //
//   // // EASY PIE CHARTS DATE FILTER
//   // //
//   // const FilterOfChartScatter = new Vue({
//   //   el: '#filter-of-chart-scatter',
//   //   template: `
//   //     <div class="row">
//   //       <div class="col-md-6 form-group">
//   //         <datetime type="datetime" :disabled="disabled" v-model="filter_start_at"></datetime>
//   //       </div>
//   //       <div class="col-md-6 form-group">
//   //         <datetime type="datetime" :disabled="disabled" v-model="filter_end_at"></datetime>
//   //       </div>
//   //     </div>
//   //   `,
//   //   data: {
//   //     filter_start_at: null,
//   //     filter_end_at: null,
//   //     chartType: 'scatter',
//   //     chartProvider: 'suspicious_to_active_addresses',
//   //     disabled: false,
//   //   },
//   //   watch: {
//   //     filter_start_at: function (value) {
//   //       if (value) {
//   //         this.updateChart(LuxonDateTime.fromISO(value).toFormat('dd.MM.yyyy HH:mm:ss'), 'filter_start_at');
//   //       }
//   //     },
//   //     filter_end_at: function (value) {
//   //       if (value) {
//   //         this.updateChart(LuxonDateTime.fromISO(value).toFormat('dd.MM.yyyy HH:mm:ss'), 'filter_end_at');
//   //       }
//   //     },
//   //   },
//   //   methods: {
//   //     updateChart: function (value, type) {
//   //       this.disabled = true;
//   //       console.log('updateChart: ', value, type);
//   //       console.log('chartProvider:', this.chartProvider);
//   //       console.log('chartType:', this.chartType);
//   //       API.dateFilter(this.chartProvider, {[type]: value})
//   //         .then((response) => {
//   //           init(response);
//   //           this.disabled = false;
//   //         })
//   //         .catch((error) => {
//   //           this.disabled = false;
//   //           console.error(error);
//   //         });
//   //     }
//   //   }
//   // })
//
// }())
