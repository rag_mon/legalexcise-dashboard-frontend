import Chart from 'chart.js';
import { COLORS } from '../../constants/colors';
import * as $ from 'jquery';


export default function (data) {
  // ------------------------------------------------------
  // @Bar Charts
  // ------------------------------------------------------
  const chartColor = {
    complaints: {
      backgroundColor: 'transparent',
      borderColor: COLORS['red-900'],
    },
    treatments: {
      backgroundColor: 'transparent',
      borderColor: COLORS['green-900'],
    },
  };




  const areaChartBox = document.getElementById(data.selector.substring(1));
  $(data.titleSelector).text(data.title);
  if (areaChartBox) {
    const areaCtx = areaChartBox.getContext('2d');

    let datasets = [];
    data.data.map((item,index)=>{
      datasets.push({
        ...chartColor[item.type],
        label           : item.label,
        borderWidth     : 1,
        data            : item.data,
      })
    });
    new Chart(areaCtx, {
      type: 'line',
      data: {
        labels: data.labels,
        datasets: datasets,
      },

      options: {
        responsive: true,
        legend: {
          position: 'bottom',
        },
      },
    });
  }
}
