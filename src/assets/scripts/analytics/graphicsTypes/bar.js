import Chart from 'chart.js';
import {COLORS} from '../../constants/colors';
import * as $ from 'jquery';


export default function (data) {
  // ------------------------------------------------------
  // @Bar Charts
  // ------------------------------------------------------
  const chartColor = {
    complaints: {
      backgroundColor: COLORS['red-500'],
      borderColor: COLORS['red-900'],
    },
    treatments: {
      backgroundColor:  COLORS['green-500'],
      borderColor: COLORS['green-900'],
    },
  };



  const barChartBox = document.getElementById(data.selector.substring(1));
  $(data.titleSelector).text(data.title);
  if (barChartBox) {
    const barCtx = barChartBox.getContext('2d');
    let datasets = [];
    data.data.map((item) => {
      datasets.push({
        ...chartColor[item.type],
        label: item.label,
        borderWidth: 1,
        data: item.data,
      })
    });
    new Chart(barCtx, {
      type: data.type,
      data: {
        labels: data.labels,
        datasets: datasets,
      },
      options: {
        responsive: true,
        legend: {
          position: 'bottom',
        },
      },
    });
  }
}
