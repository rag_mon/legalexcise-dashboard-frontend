import Chart from 'chart.js';
import { COLORS } from '../../constants/colors';
import * as $ from 'jquery';


export default function (data) {
  const chartColor = {
    complaints: {
      backgroundColor: 'transparent',
      borderColor: COLORS['red-900'],
    },
    treatments: {
      backgroundColor: 'transparent',
      borderColor: COLORS['green-900'],
    },
  };




  const lineChartBox = document.getElementById(data.selector.substring(1));
  $(data.titleSelector).text(data.title);
  if (lineChartBox) {
    const lineCtx = lineChartBox.getContext('2d');
    lineChartBox.height = 80;
    let datasets = [];
    data.data.map((item,index)=>{
      datasets.push({
        ...chartColor[item.type],
        label           : item.label,
        borderWidth     : 2,
        data            : item.data,
      })
    });
    new Chart(lineCtx, {
      type: data.type,
      data: {
        labels: data.labels,
        datasets: datasets,
      },

      options: {
        responsive: true,
        legend: {
          position: 'bottom',
        },
      },
    });
  }
}
