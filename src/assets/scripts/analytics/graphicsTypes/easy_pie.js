import * as $ from 'jquery';
import 'easy-pie-chart/dist/jquery.easypiechart.min.js';

export default function (data) {



  let pieArrayNode = [];
  $(data.titleSelector).text(data.title);
  data.data.map((item) => {
    pieArrayNode.push(`<div class="peer">
                    <div class="easy-pie-chart" data-size='80' data-percent="${item.data}" data-bar-color='#f44336'>
                      <span></span>
                    </div>
                    <h6 class="fsz-sm">${item.label}</h6>
                  </div>`)
  });
  $(data.selector).append(pieArrayNode);
  if ($('.easy-pie-chart').length > 0) {
    $('.easy-pie-chart').easyPieChart({
      onStep(from, to, percent) {
        this.el.children[0].innerHTML = `${Math.round(percent)} %`;
      },
    });
  }
}

