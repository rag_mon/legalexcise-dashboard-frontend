import Chart from 'chart.js';
import {COLORS} from '../../constants/colors';
import * as $ from 'jquery';


export default function (data) {
  // ------------------------------------------------------
  // @Bar Charts
  // ------------------------------------------------------
  const chartColor = {
    complaints: {
      backgroundColor: 'transparent',
      borderColor: COLORS['red-900'],
    },
    treatments: {
      backgroundColor: 'transparent',
      borderColor: COLORS['green-900'],
    },
  };

  // ------------------------------------------------------
  // @Scatter Charts
  // ------------------------------------------------------

  const scatterChartBox = document.getElementById(data.selector.substring(1));
  $(data.titleSelector).text(data.title);
  if (scatterChartBox) {
    const scatterCtx = scatterChartBox.getContext('2d');
    let datasets = [];
    data.data.map((item, index) => {
      datasets.push({
        label: item.label,
        ...chartColor[item.type],
        borderWidth: 1,
        data: item.data,
      })
    });
    Chart.Scatter(scatterCtx, {
      data: {
        datasets: datasets
      },
    });
  }

}
