export default function () {
  window.charts = [
    {
      selector: '#selector-of-chart-bar',
      filter: '#filter-of-chart-bar',
      filterProvider: 'adding_addresses',
      type: 'bar', // тип графика
      titleSelector: '#title-of-chart-bar',
      title: 'Статистика жалоб/обработок',
      labels: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
      data: [
        {
          type: 'complaints',
          label: 'Жалобы',
          data: [60, 50, 70, 60, 50, 70, 60],
        }, {
          type: 'treatments',
          label: 'Обработка',
          data: [70, 20.5, 75, 85, 70, 75, 85, 70],
        }
      ]
    }, {
      selector: '#selector-of-chart-line',
      filter: '#filter-of-chart-line',
      filterProvider: 'complaint_reactions',
      type: 'line', // тип графика
      titleSelector: '#title-of-chart-line',
      title: 'Статистика жалоб/обработок',
      labels: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
      data: [
        {
          type: 'complaints',
          label: 'Жалобы',
          data: [60, 50, 70, 60, 50, 70, 60],
        }, {
          type: 'treatments',
          label: 'Обработка',
          data: [70, 20.5, 75, 85, 70, 75, 85, 70],
        }
      ]
    }, {
      selector: '#selector-of-chart-area',
      filter: '#filter-of-chart-area',
      type: 'area', // тип графика
      titleSelector: '#title-of-chart-area',
      title: 'Статистика жалоб/обработок',
      labels: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
      data: [
        {
          type: 'complaints',
          label: 'Жалобы',
          data: [60, 50, 70, 60, 50, 70, 60],
        }, {
          type: 'treatments',
          label: 'Обработка',
          data: [70, 20.5, 75, 85, 70, 75, 85, 70],
        }
      ]
    },{
      selector: '#selector-of-chart-scatter',
      filter: '#filter-of-chart-scatter',
      type: 'scatter', // тип графика
      titleSelector: '#title-of-chart-scatter',
      title: 'Статистика жалоб/обработок',
      labels: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
      data: [
        {
          type: 'complaints',
          label: 'Жалобы',
          data: [
            { x: 10, y: 20 },
            { x: 30, y: 40 },
            { x: 50, y: 60 },
            { x: 70, y: 80 },
            { x: 90, y: 100 },
            { x: 110, y: 120 },
            { x: 130, y: 140 },
          ],
        }, {
          type: 'treatments',
          label: 'Обработка',
          data: [
            { x: 150, y: 160 },
            { x: 170, y: 180 },
            { x: 190, y: 200 },
            { x: 210, y: 220 },
            { x: 230, y: 240 },
            { x: 250, y: 260 },
            { x: 270, y: 280 },
          ],
        }
      ]
    }, {
      selector: '#selector-of-chart-easy-pie',
      filter: '#filter-of-chart-easy-pie',
      filterProvider: 'percent_reactions',
      type: 'easy_pie', // тип графика
      titleSelector: '#title-of-chart-easy-pie',
      title: 'Статистика жалоб/обработок',
      data: [
        {
          type: 'complaints',
          label: 'Жалобы',
          data: 60,
        }, {
          type: 'treatments',
          label: 'Обработка',
          data: 40,
        }
      ]
    }, {
      selector: '#selector-of-chart-sparkline',
      filter: '#filter-of-chart-sparkline',
      filterProvider: 'suspicious_to_active_addresses',
      type: 'sparkline', // тип графика
      titleSelector: '#title-of-chart-sparkline',
      title: 'Статистика жалоб/обработок',
      data: [5, 4, 5, -2, 0, 3, -5, 6, 7, 9, 9, 5, -3, -2, 2, -4]
    },
  ];



  window.counter = [
      {
          selector: '#selector-of-count1',
          type: 'counter',
          filter: '#filter-of-count1',
          filterProvider: 'suspicious_to_active_addresses',
          data: 100,
      }, {
          selector: '#selector-of-count2',
          type: 'counter',
          filter: '#filter-of-count2',
          filterProvider: 'suspicious_to_active_addresses',
          data: 150,
      }, {
          selector: '#selector-of-count3',
          type: 'counter',
          filter: '#filter-of-count3',
          filterProvider: 'suspicious_to_active_addresses',
          data: 160,
      }
  ]



}
