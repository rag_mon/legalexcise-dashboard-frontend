import Vue from 'vue'
// You need a specific loader for CSS files
import 'vue-datetime/dist/vue-datetime.css'
import {DateTime as LuxonDateTime, Duration} from 'luxon'
import {Datetime} from 'vue-datetime';
import API from './api';
import {CounterBlock} from './graphicsTypes/index'
import * as $ from 'jquery';

export const FILTER_COUNTER_INIT = (date) => {

  const initChart = (chart) => {
    switch (chart.type) {
      case('counter'): {
        CounterBlock(chart);
        break
      }
    }
  };
  if(!$(date.filter).length) return null

  Vue.component('datetime', Datetime);
  window.LuxonDateTime = LuxonDateTime;

  const FilterOfChart = new Vue({
    el: date.filter,
    template: `
      <div class="filter-counter">
      
        <div class="form-group input-group flex-nowrap">
          <div class="input-group-prepend">
            <span class="input-group-text" id="inputGroup-sizing-default">От</span>
          </div>
          <datetime input-class="form-control" type="datetime" :disabled="disabled" v-model="filter_start_at"></datetime>          
        </div>
        
        <div class="form-group input-group flex-nowrap">
          <div class="input-group-prepend">
            <span class="input-group-text" id="inputGroup-sizing-default">До</span>
          </div>
          <datetime input-class="form-control" type="datetime" :disabled="disabled" v-model="filter_end_at"></datetime>
        <div  v-bind:class="{ 'invalid-tooltip': validMessage }">{{validMessage}}</div>
        </div>
      </div>
    `,
    data: {
      filter_start_at: null,
      filter_end_at: null,
      maxDatetime: LuxonDateTime.local().toISO(),
      chartType: date.type,
      chartProvider: date.filterProvider,
      disabled: false,
      validMessage: '',
    },
    watch: {
      filter_start_at: function (value) {
        if (value) {
          this.updateChart(LuxonDateTime.fromISO(value), 'from');
        }
      },
      filter_end_at: function (value) {
        if (value) {
          this.updateChart(LuxonDateTime.fromISO(value), 'to');
        }
      },
    },
    methods: {
      updateChart: function (value, type) {
        let filter = '';
        let filter_start_at = LuxonDateTime.fromISO(this.filter_start_at).toFormat('dd.MM.yyyy HH:mm:ss');
        let filter_end_at = LuxonDateTime.fromISO(this.filter_end_at).toFormat('dd.MM.yyyy HH:mm:ss');

        console.log(filter_start_at);
        console.log(filter_end_at);

        if (this.filter_start_at || this.filter_end_at) {


          // if (new Date(this.filter_start_at).getTime() < new Date(this.filter_end_at).getTime()) {
          //
          //   this.validMessage = '';
          //
          // } else {
          //   this.validMessage = 'Неверно задан интервал';
          //   return
          // }
          this.disabled = true;

          if (this.filter_start_at && this.filter_end_at) {
            filter = `?from=${filter_start_at}&to=${filter_end_at}`;

          } else if (this.filter_end_at) {
            filter = `?to=${filter_end_at}`;

          } else if (this.filter_start_at) {
            filter = `?from=${filter_start_at}`;
          }
          console.log(filter);


          API.dateFilterCounter(this.chartProvider, filter)
            .then((response) => {
              console.log(response);
              this.disabled = false;
              return response.text().then(function(text) {
                console.log(text);
                $(date.selector).val(text);
              });

            })
            .catch((error) => {
              this.disabled = false;
              console.error(error);
            });

        } else {
          return null
        }
      }
    }
  });


}


// export default (function () {
//
//   Vue.component('datetime', Datetime);
//   window.LuxonDateTime = LuxonDateTime;
//
//   const FilterOfChartArea = new Vue({
//     el: '#filter-of-chart-bar',
//     template: `
//       <div class="row">
//         <div class="col-md-6 form-group">
//           <datetime type="datetime" :disabled="disabled" v-model="filter_start_at"></datetime>
//         </div>
//         <div class="col-md-6 form-group">
//           <datetime type="datetime" :disabled="disabled" v-model="filter_end_at"></datetime>
//         </div>
//       </div>
//     `,
//     data: {
//       filter_start_at: null,
//       filter_end_at: null,
//       chartType: 'bar',
//       chartProvider: 'adding_addresses',
//       disabled: false,
//     },
//     watch: {
//       filter_start_at: function (value) {
//         if (value) {
//           this.updateChart(LuxonDateTime.fromISO(value).toFormat('dd.MM.yyyy HH:mm:ss'), 'filter_start_at');
//         }
//       },
//       filter_end_at: function (value) {
//         if (value) {
//           this.updateChart(LuxonDateTime.fromISO(value).toFormat('dd.MM.yyyy HH:mm:ss'), 'filter_end_at');
//         }
//       },
//     },
//     methods: {
//       updateChart: function (value, type) {
//         this.disabled = true;
//         console.log('updateChart: ', value, type);
//         console.log('chartProvider:', this.chartProvider);
//         console.log('chartType:', this.chartType);
//         API.dateFilter(this.chartProvider, {[type]: value})
//           .then((response) => {
//             initChart(response);
//             this.disabled = false;
//           })
//           .catch((error) => {
//             this.disabled = false;
//             console.error(error);
//           });
//       }
//     }
//   });
//
//
//
//
//   //
//   // BAR-CHART DATE FILTER
//   //
//   // const FilterOfChartArea = new Vue({
//   //   el: '#filter-of-chart-bar',
//   //   template: `
//   //     <div class="row">
//   //       <div class="col-md-6 form-group">
//   //         <datetime type="datetime" :disabled="disabled" v-model="filter_start_at"></datetime>
//   //       </div>
//   //       <div class="col-md-6 form-group">
//   //         <datetime type="datetime" :disabled="disabled" v-model="filter_end_at"></datetime>
//   //       </div>
//   //     </div>
//   //   `,
//   //   data: {
//   //     filter_start_at: null,
//   //     filter_end_at: null,
//   //     chartType: 'bar',
//   //     chartProvider: 'adding_addresses',
//   //     disabled: false,
//   //   },
//   //   watch: {
//   //     filter_start_at: function (value) {
//   //       if (value) {
//   //         this.updateChart(LuxonDateTime.fromISO(value).toFormat('dd.MM.yyyy HH:mm:ss'), 'filter_start_at');
//   //       }
//   //     },
//   //     filter_end_at: function (value) {
//   //       if (value) {
//   //         this.updateChart(LuxonDateTime.fromISO(value).toFormat('dd.MM.yyyy HH:mm:ss'), 'filter_end_at');
//   //       }
//   //     },
//   //   },
//   //   methods: {
//   //     updateChart: function (value, type) {
//   //       this.disabled = true;
//   //       console.log('updateChart: ', value, type);
//   //       console.log('chartProvider:', this.chartProvider);
//   //       console.log('chartType:', this.chartType);
//   //       API.dateFilter(this.chartProvider, {[type]: value})
//   //         .then((response) => {
//   //           initChart(response);
//   //           this.disabled = false;
//   //         })
//   //         .catch((error) => {
//   //           this.disabled = false;
//   //           console.error(error);
//   //         });
//   //     }
//   //   }
//   // });
//
//   // //
//   // // BAR-CHART DATE FILTER
//   // //
//   // const FilterOfChartBar = new Vue({
//   //   el: '#filter-of-chart-bar',
//   //   template: `
//   //     <div class="row">
//   //       <div class="col-md-6 form-group">
//   //         <datetime type="datetime" :disabled="disabled" v-model="filter_start_at"></datetime>
//   //       </div>
//   //       <div class="col-md-6 form-group">
//   //         <datetime type="datetime" :disabled="disabled" v-model="filter_end_at"></datetime>
//   //       </div>
//   //     </div>
//   //   `,
//   //   data: {
//   //     filter_start_at: null,
//   //     filter_end_at: null,
//   //     chartType: 'bar',
//   //     chartProvider: 'adding_addresses',
//   //     disabled: false,
//   //   },
//   //   watch: {
//   //     filter_start_at: function (value) {
//   //       if (value) {
//   //         this.updateChart(LuxonDateTime.fromISO(value).toFormat('dd.MM.yyyy HH:mm:ss'), 'filter_start_at');
//   //       }
//   //     },
//   //     filter_end_at: function (value) {
//   //       if (value) {
//   //         this.updateChart(LuxonDateTime.fromISO(value).toFormat('dd.MM.yyyy HH:mm:ss'), 'filter_end_at');
//   //       }
//   //     },
//   //   },
//   //   methods: {
//   //     updateChart: function (value, type) {
//   //       this.disabled = true;
//   //       console.log('updateChart: ', value, type);
//   //       console.log('chartProvider:', this.chartProvider);
//   //       console.log('chartType:', this.chartType);
//   //       API.dateFilter(this.chartProvider, {[type]: value})
//   //         .then((response) => {
//   //           initChart(response);
//   //           this.disabled = false;
//   //         })
//   //         .catch((error) => {
//   //           this.disabled = false;
//   //           console.error(error);
//   //         });
//   //     }
//   //   }
//   // });
//   //
//   //
//   //
//   // //
//   // // LINE-CHART DATE FILTER
//   // //
//   // const FilterOfChartLine = new Vue({
//   //   el: '#filter-of-chart-line',
//   //   template: `
//   //     <div class="row">
//   //       <div class="col-md-6 form-group">
//   //         <datetime type="datetime" :disabled="disabled" v-model="filter_start_at"></datetime>
//   //       </div>
//   //       <div class="col-md-6 form-group">
//   //         <datetime type="datetime" :disabled="disabled" v-model="filter_end_at"></datetime>
//   //       </div>
//   //     </div>
//   //   `,
//   //   data: {
//   //     filter_start_at: null,
//   //     filter_end_at: null,
//   //     chartType: 'line',
//   //     chartProvider: 'complaint_reactions',
//   //     disabled: false,
//   //   },
//   //   watch: {
//   //     filter_start_at: function (value) {
//   //       if (value) {
//   //         this.updateChart(LuxonDateTime.fromISO(value).toFormat('dd.MM.yyyy HH:mm:ss'), 'filter_start_at');
//   //       }
//   //     },
//   //     filter_end_at: function (value) {
//   //       if (value) {
//   //         this.updateChart(LuxonDateTime.fromISO(value).toFormat('dd.MM.yyyy HH:mm:ss'), 'filter_end_at');
//   //       }
//   //     },
//   //   },
//   //   methods: {
//   //     updateChart: function (value, type) {
//   //       this.disabled = true;
//   //       console.log('updateChart: ', value, type);
//   //       console.log('chartProvider:', this.chartProvider);
//   //       console.log('chartType:', this.chartType);
//   //       API.dateFilter(this.chartProvider, {[type]: value})
//   //         .then((response) => {
//   //           init(response);
//   //           this.disabled = false;
//   //         })
//   //         .catch((error) => {
//   //           this.disabled = false;
//   //           console.error(error);
//   //         });
//   //     }
//   //   }
//   // });
//   //
//   //
//   // //
//   // // EASY PIE CHARTS DATE FILTER
//   // //
//   // const FilterOfChartEasyPie = new Vue({
//   //   el: '#filter-of-chart-line',
//   //   template: `
//   //     <div class="row">
//   //       <div class="col-md-6 form-group">
//   //         <datetime type="datetime" :disabled="disabled" v-model="filter_start_at"></datetime>
//   //       </div>
//   //       <div class="col-md-6 form-group">
//   //         <datetime type="datetime" :disabled="disabled" v-model="filter_end_at"></datetime>
//   //       </div>
//   //     </div>
//   //   `,
//   //   data: {
//   //     filter_start_at: null,
//   //     filter_end_at: null,
//   //     chartType: 'spark',
//   //     chartProvider: 'suspicious_to_active_addresses',
//   //     disabled: false,
//   //   },
//   //   watch: {
//   //     filter_start_at: function (value) {
//   //       if (value) {
//   //         this.updateChart(LuxonDateTime.fromISO(value).toFormat('dd.MM.yyyy HH:mm:ss'), 'filter_start_at');
//   //       }
//   //     },
//   //     filter_end_at: function (value) {
//   //       if (value) {
//   //         this.updateChart(LuxonDateTime.fromISO(value).toFormat('dd.MM.yyyy HH:mm:ss'), 'filter_end_at');
//   //       }
//   //     },
//   //   },
//   //   methods: {
//   //     updateChart: function (value, type) {
//   //       this.disabled = true;
//   //       console.log('updateChart: ', value, type);
//   //       console.log('chartProvider:', this.chartProvider);
//   //       console.log('chartType:', this.chartType);
//   //       API.dateFilter(this.chartProvider, {[type]: value})
//   //         .then((response) => {
//   //           init(response);
//   //           this.disabled = false;
//   //         })
//   //         .catch((error) => {
//   //           this.disabled = false;
//   //           console.error(error);
//   //         });
//   //     }
//   //   }
//   // });
//   //
//   // //
//   // // EASY PIE CHARTS DATE FILTER
//   // //
//   // const FilterOfChartScatter = new Vue({
//   //   el: '#filter-of-chart-scatter',
//   //   template: `
//   //     <div class="row">
//   //       <div class="col-md-6 form-group">
//   //         <datetime type="datetime" :disabled="disabled" v-model="filter_start_at"></datetime>
//   //       </div>
//   //       <div class="col-md-6 form-group">
//   //         <datetime type="datetime" :disabled="disabled" v-model="filter_end_at"></datetime>
//   //       </div>
//   //     </div>
//   //   `,
//   //   data: {
//   //     filter_start_at: null,
//   //     filter_end_at: null,
//   //     chartType: 'scatter',
//   //     chartProvider: 'suspicious_to_active_addresses',
//   //     disabled: false,
//   //   },
//   //   watch: {
//   //     filter_start_at: function (value) {
//   //       if (value) {
//   //         this.updateChart(LuxonDateTime.fromISO(value).toFormat('dd.MM.yyyy HH:mm:ss'), 'filter_start_at');
//   //       }
//   //     },
//   //     filter_end_at: function (value) {
//   //       if (value) {
//   //         this.updateChart(LuxonDateTime.fromISO(value).toFormat('dd.MM.yyyy HH:mm:ss'), 'filter_end_at');
//   //       }
//   //     },
//   //   },
//   //   methods: {
//   //     updateChart: function (value, type) {
//   //       this.disabled = true;
//   //       console.log('updateChart: ', value, type);
//   //       console.log('chartProvider:', this.chartProvider);
//   //       console.log('chartType:', this.chartType);
//   //       API.dateFilter(this.chartProvider, {[type]: value})
//   //         .then((response) => {
//   //           init(response);
//   //           this.disabled = false;
//   //         })
//   //         .catch((error) => {
//   //           this.disabled = false;
//   //           console.error(error);
//   //         });
//   //     }
//   //   }
//   // })
//
// }())
