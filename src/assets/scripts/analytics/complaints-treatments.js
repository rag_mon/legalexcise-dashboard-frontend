import {
  AreaChart,
  BarChart,
  EasyPieChart,
  LineChart,
  ScatterChart,
  SparkLineChart,
  CounterBlock
} from './graphicsTypes/index'
import mock from './mock'

import API from './api';
import {FILTER_INIT} from "./vueFilterDate";
import {FILTER_COUNTER_INIT} from "./vueFilterDateCounter";

export default (function () {
  console.log(window.charts);
  if (process.env.NODE_ENV === 'development') {
    mock()
  }
  const init = (chart) => {
    switch (chart.type) {
      case('area'): {
        AreaChart(chart);
        break
      }
      case('bar'): {
        BarChart(chart);
        break
      }
      case('easy_pie'): {
        EasyPieChart(chart);
        break
      }
      case('line'): {
        LineChart(chart);
        break
      }
      case('scatter'): {
        ScatterChart(chart);
        break
      }
      case('counter'): {
        CounterBlock(chart);
        break
      }
    }
  };


  window.charts && window.charts.map((item) => {
    init(item);
    if (item.filter && item.filterProvider) {
      FILTER_INIT(item);
    }
  });
  window.counter && window.counter.map((item) => {
    init(item);
    if (item.filter && item.filterProvider) {
      FILTER_COUNTER_INIT(item);
    }
  });


}())
