import * as $ from "jquery";
import {fetchAPI} from '../utils/fetch';

export default {
  dateFilter(path,body) {
    return new Promise((resolve, reject) => {
      fetchAPI(`/analytics/${path}`,'POST')
        .then((response) => {
          return response.json()
        })
        .then((response) => {
          resolve(response);
        });
    })
  },

  dateFilterCounter(PROVIDER,filter) {
    return new Promise((resolve, reject) => {
      fetchAPI(`/hack/anal/count/${PROVIDER}${filter}`,'get')
        .then((response) => {
          resolve(response);
        })
    })
  },


}
