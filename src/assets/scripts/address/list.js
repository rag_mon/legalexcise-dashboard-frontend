import * as $ from 'jquery';
import DataTableAPI from '../datatable/api';
import API from './api';
import addComment from './addComment';
import deleteAddress from './deleteAddress';
import moment from 'moment';

export default (function () {


  const $modalAddressDeleteConfirm = $('#modalAddressDeleteConfirm');
  const $modalAddressAddComment = $('#modalAddressAddComment');

  const $modalPreLoader = $('#modalPreLoader');
  const $modalSubmitMessage = $('#modalSubmitMessage');
  const lexicon = {

    RU: {
      dropdawn: {
        action: 'Действие',
        addComment: 'Добавить комментарий',
        view: 'Просмотреть',
        management: 'Управление',
        delete: 'Удалить',
      },


    },
    UK: {
      dropdawn: {
        action: 'Дія',
        addComment: 'Додати коментар',
        view: 'Переглянути',
        management: 'Управління',
        delete: 'Видалити',
      },
    }
  };

  let currentLocal = ( window.config && window.config.lang) || 'RU';
  currentLocal = currentLocal.toUpperCase();

  if (currentLocal !== 'RU' || currentLocal !== 'UK') {
    currentLocal = 'RU';
  }

  console.log($('#addressListTable'));
  if ($('#addressListTable').length) {
    const $addressListTable = $('#addressListTable')
      .on('init.dt', function () {
        console.log('Table initialisation complete');
        setTimeout(() => {
          $addressListTable.draw();
        }, 1000)
      })
      .DataTable({
        paging: true,
        searching: true,

        stateSave: true,
        ordering: true,
        info: true,
        scrollX: true,
        "processing": true,
        "serverSide": true,
        "drawCallback": function () {
          console.log('drawCallback');
          //
          deleteAddress($modalAddressDeleteConfirm, $modalSubmitMessage, $addressListTable);
          //
          addComment($modalAddressAddComment, $modalSubmitMessage, $addressListTable);

        },
        ...(process.env.NODE_ENV === 'development'? {
          "ajax": "http://localhost:4000/address",
        }:{
          "ajax": "/address",
        }),
        "columns": [
          {
              "name": "address",
              "orderable": true,
              "searchable": true,
            "render": function (data, type, row) {
              return row.address;
            }
          },
          {
              "orderable": true,
              "searchable": true,
            "render": function (data, type, row) {
              return row.license;
            }
          },
          {
              "orderable": true,
              "searchable": true,
            "render": function (data, type, row) {
              return row.license_start_at;
            }
          },
          {
              "orderable": true,
              "searchable": true,
            "render": function (data, type, row) {
              return row.license_end_at;
            }
          },
          {
              "orderable": true,
              "searchable": true,
            "render": function (data, type, row) {
              return row.status;
            }
          },
          {
            "render": function (data, type, row) {

              return ` <div class="dropdown">
        <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuButton"
                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          ${lexicon[currentLocal].dropdawn.action}
        </button>
        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">

          <a class="dropdown-item dropdown-item-add-comment" href="#"
             data-toggle="modal"
             data-address-id="${row.id}"
             data-title="${row.address}"
             data-target="#modalAddressAddComment"
          >
            ${lexicon[currentLocal].dropdawn.addComment}
          </a>

          <a class="dropdown-item" href="/address/${row.id}">${lexicon[currentLocal].dropdawn.view}</a>
          <a class="dropdown-item" target="_blank" href="https://maps.google.com/maps?q=loc:${row.lat},${row.lng}">На
            карте</a>
          <a class="dropdown-item" href="/address/${row.id}/edit">${lexicon[currentLocal].dropdawn.management}</a>
          
          <a class="dropdown-item dropdown-item-delete" href="#"
             data-toggle="modal"
             data-address-id="${row.id}"
             data-target="#modalAddressDeleteConfirm"
             data-title="${row.address}">${lexicon[currentLocal].dropdawn.delete}</a>
        </div>
      </div>`;
            }
          }
        ],
        language: {
          url: DataTableAPI.getLanguageUrl(),
        }
      })
    // .column( 1 ).search(
    //   $('#addressListTable_filter input').val(),
    //   true,
    //   false,
    // ).draw();




  } else {

    deleteAddress($modalAddressDeleteConfirm, $modalSubmitMessage, null);
    //
    addComment($modalAddressAddComment, $modalSubmitMessage, null);
  }


}());
