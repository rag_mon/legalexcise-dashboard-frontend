import * as $ from 'jquery';
import {ValidationForm} from './validation';
import {formConfig} from './formConfig';
import API from './api';

export default (function () {
    // нам нужно привязываться на 2 типа форм: создания и управления
    const $formAddressEdit = $('#formAddressCreate, #formAddressEdit');
    console.log('address form (variants: create/edit):', $formAddressEdit);
    const $modalSubmitMessage = $('#modalSubmitMessage');
    const lexicon = {

        RU: {
            button: {
                updates: 'Издать обновление',
            },
            modalSubmit: {
                success: {
                    title: 'Успех',
                    message: (ADDRESS) => `Адрес <b>“${ADDRESS}”</b> сохранён.`,
                },
                error: {
                    title: 'Ошибка',
                    message: `Произошла непредвиденная ошибка. Попробуйте перезагрузить страницу и повторить запрос.`,
                },
            }

        },
        UK: {
            button: {
                updates: 'Видати оновлення',
            },
            modalSubmit: {
                success: {
                    title: 'Успіх',
                    message: (ADDRESS) => `Адреса <b>“${ADDRESS}”</b> сохранён.`,
                },
                error: {
                    title: 'Помилка',
                    message: `Сталася неочікувана помилка. Спробуйте перезавантажити сторінку і повторити запит.`,
                },
            }
        }
    };

    let currentLocal = ( window.config && window.config.lang) || 'RU';
    currentLocal = currentLocal.toUpperCase();

    if (currentLocal !== 'RU' || currentLocal !== 'UK') {
        currentLocal = 'RU';
    }

    let markersArray = [];
    $('.address-submit').unbind('click').bind('click', function (e) {
        const ID = $(this).attr('data-address-id');
        const ADDRESS = $(this).attr('data-title');
        const REDIRECT = $(this).attr('data-redirect');
        const SAVE_TYPE = $(this).attr('data-save-type');
        console.log(REDIRECT);
        console.log(ADDRESS);
        console.log(ID);
        if (ValidationForm($formAddressEdit, formConfig)) {
            const formData = {
                public_notices: {}
            };
            formConfig.map((item) => {
                if (item.name === 'confiscated_goods') {
                    formData['public_notices'][item.name] = $($formAddressEdit.find(`[name="${item.name}"]`)[0]).val();
                } else if (item.name === 'protocol_drawn_up') {
                    formData['public_notices'][item.name] = $($formAddressEdit.find(`[name="${item.name}"]`)[0]).val();
                } else if (item.name === 'financial_sanctions') {
                    formData['public_notices'][item.name] = $($formAddressEdit.find(`[name="${item.name}"]`)[0]).val();
                } else {
                    formData[item.name] = $($formAddressEdit.find(`[name="${item.name}"]`)[0]).val();
                }
            });
            formData['public_notices'] = JSON.stringify(formData['public_notices']);
            console.log(formData);
            if (ID) {
                API.saveAddress(ID, formData, REDIRECT)
                    .then((response) => {
                        console.log(response, REDIRECT);
                        // $modalPreLoader.modal('hide');
                        $modalSubmitMessage.find('#modalSubmitMessageLabel').html(lexicon[currentLocal].modalSubmit.success.title);
                        $modalSubmitMessage.find('.data-message-value').html(lexicon[currentLocal].modalSubmit.success.message(ADDRESS));
                        $modalSubmitMessage.modal('show');

                        if (REDIRECT) {
                          // redirection timeout
                          setTimeout(() => window.location.replace(REDIRECT), 3000);
                        }
                    })
                    .catch(err => {
                        console.error(err);
                        // $modalPreLoader.modal('hide');
                        $modalSubmitMessage.find('#modalSubmitMessageLabel').html(lexicon[currentLocal].modalSubmit.error.title);
                        $modalSubmitMessage.find('.data-message-value').html(lexicon[currentLocal].modalSubmit.error.message);
                        $modalSubmitMessage.modal('show');
                    })
            } else {

                API.createAddress(formData, REDIRECT)
                    .then((response) => {
                        console.log(response);
                        // $modalPreLoader.modal('hide');
                        $modalSubmitMessage.find('#modalSubmitMessageLabel').html(lexicon[currentLocal].modalSubmit.success.title);
                        $modalSubmitMessage.find('.data-message-value').html(lexicon[currentLocal].modalSubmit.success.message(ADDRESS));
                        $modalSubmitMessage.modal('show');

                        // при успешно созданом адресе редеректить нужно в любом случае на другую страницу
                        let redirectUrl = (SAVE_TYPE == 'save') ? REDIRECT.replace('%ADDRESS_ID%', response.id) : REDIRECT;
                        console.log('createAddress redirectUrl:', redirectUrl);
                        setTimeout(() => window.location.replace(redirectUrl), 3000);
                    })
                    .catch(err => {
                        console.error(err);
                        // $modalPreLoader.modal('hide');
                        $modalSubmitMessage.find('#modalSubmitMessageLabel').html(lexicon[currentLocal].modalSubmit.error.title);
                        $modalSubmitMessage.find('.data-message-value').html(lexicon[currentLocal].modalSubmit.error.message);
                        $modalSubmitMessage.modal('show');
                    })
            }


        }


    });

    // GoogleMap


    $('#get-cords-from-map').on('click', function (e) {
        const map = window.map;
        const $inputLat = $('#inputLat');
        const $inputLong = $('#inputLong');
        const $getCordsResult = $('#get-cords-result');

        map.addListener('click', function (event) {
            console.log(event);
            const latlng = {
                lat: event.latLng.lat(),
                lng: event.latLng.lng(),
            };

            for (let i = 0; i < markersArray.length; i++) {
                markersArray[i].setMap(null);
            }
            markersArray.length = 0;

            markersArray.push(new google.maps.Marker({
                position: latlng,
                map: map
            }));

            console.log(latlng);
            $inputLat.val(latlng.lat);
            $inputLong.val(latlng.lng);
            $getCordsResult.html(`Широта: <b>${latlng.lat}</b>, Долгота<b>${latlng.lng}</b>`)
        });
    })
}());



