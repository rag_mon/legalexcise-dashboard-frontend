import * as $ from 'jquery';
import {fetchAPI} from '../utils/fetch';


export default {

  deleteAddress(id) {
    return new Promise((resolve, reject) => {
      fetchAPI(`/address/${id}`, 'DELETE')
        .then((response) => {
          return response.json()
        })
        .then((response) => {
          resolve(response);
        })
        .catch((err) => {
          reject(err);
        });
    })
  },

  addComment(id, message) {
    return new Promise((resolve, reject) => {
      fetchAPI(`/address/${id}/comment`, 'post')
        .then((response) => {
          return response.json()
        })
        .then((response) => {
          resolve(response);
        })
        .catch((err) => {
          reject(err);
        });
    })
  },

  saveAddress(id, formData, redirect) {
    return new Promise((resolve, reject) => {
      // здесь добавлялся редирект "?redirect_to=" GET параметров в запросе, но от него толку нету.
      // и в определённых ситуациях это лишь создаёт проблемы
      // к примеру, если "redirect" аргумент текущей функции не задан,
      // то тогда итоговый URL на который будет отправлен запрос "/address/"
      // но, сервер не примет такой запрос на такой URL, он сделает авто-редирект.
      // который в свою очередь на фронте не корректно обрабатывался после натяжки на backend
      fetchAPI(`/address/${id}`, 'put',formData)
        .then((response) => {
          return response.json()
        })
        .then((response) => {
          resolve(response);
        })
        .catch((err) => {
          reject(err);
        });
    })
  },

  createAddress(formData, redirect) {
    return new Promise((resolve, reject) => {
      // здесь добавлялся редирект "?redirect_to=" GET параметров в запросе, но от него толку нету.
      // и в определённых ситуациях это лишь создаёт проблемы
      // к примеру, если "redirect" аргумент текущей функции не задан,
      // то тогда итоговый URL на который будет отправлен запрос "/address/"
      // но, сервер не примет такой запрос на такой URL, он сделает авто-редирект.
      // который в свою очередь на фронте не корректно обрабатывался после натяжки на backend
      fetchAPI(`/address`, 'post',formData)
        .then((response) => {
          return response.json()
        })
        .then((response) => {
          resolve(response);
        })
        .catch((err) => {
          reject(err);
        });
    })
  },
};
