let currentLocal = ( window.config && window.config.lang) || 'RU';
currentLocal = currentLocal.toUpperCase();

if (currentLocal !== 'RU' || currentLocal !== 'UK') {
  currentLocal = 'RU';
}

const lexicon = {
  RU: {
    required: 'Поле обязательно для заполнения',
    isNumber: 'Только число',
    maxLength: (max) => `Максимальное кол-во символов ${max}.`,
    Latitude: 'Неверный формат записи шыроты',
    Longtude: 'Неверный формат записи долготы',
  },
  UK: {
    required: "Поле обов'язкове для заповнення",
    isNumber: 'Тільки число',
    maxLength: (max) => `Максимальна кількість символів ${max}.`,
    Latitude: 'Невірний формат запису широти',
    Longtude: 'Невірний формат запису довготи',
  }
};


export const required = (value) => value ? undefined : lexicon[currentLocal].required;
export const isNumber = value => {
  console.log(value);
  console.log(value && isNaN(value));
  return value && isNaN(value) ? lexicon[currentLocal].isNumber : undefined
};

export const maxLength = max => value => value && value.length > max ? lexicon[currentLocal].maxLength(max) : undefined;
export const Latitude = value => value && value.match(/^-?\d*(\.\d+)?$/) ? undefined : lexicon[currentLocal].Latitude;
export const Longtude = value => value && value.match(/^-?\d*(\.\d+)?$/) ? undefined : lexicon[currentLocal].Longtude;


/*
* @param (object) $form - DOM element
* @param (array) inputs - array object
* @param (string) inputs[0].id - input id
* @param (string) inputs[0].name - input name
* @param (array) inputs[0].validate - array of validation functions
* */
export const ValidationForm = ($form, inputs) => {


  let form_valid_status = [];

  inputs.map((item) => {
    if (item.validate.length) {
      const $input = $($form.find(`[name="${item.name}"]`)[0]);
      const value = $input.val();
      const errors = [];
      if (item.required || value) {
        item.validate.map((fn) => {
          if (fn(value)) {
            errors.push(fn(value));
          }
        });

        if (errors.length) {
          console.log(`has validation errors for input: ${item.name}`);
          const $invalid_feedback = $input.next(".invalid-feedback");
          if ($invalid_feedback) {
            console.log('invalid_feedback element is set', $invalid_feedback);
            // errors
            $invalid_feedback.empty();
            let msg_nodes = '';
            errors.map((msg) => {
              msg_nodes += `<div>${msg}</div>`;
            });
            $invalid_feedback.append(msg_nodes);
            console.log(`append invalid_feedback by msg_nodes`, msg_nodes);
          }
          form_valid_status.push(false);
          $input.removeClass('is-valid').addClass('is-invalid');
        } else {
          form_valid_status.push(true);
          $input.removeClass('is-invalid').addClass('is-valid');
        }
      }
    } else {

    }

  });
  return findFalse(form_valid_status, false)
};

export const findFalse = (array, value) => {

  for (let i = 0; i < array.length; i++) {
    if (array[i] === value) return false;
  }

  return true;
};

