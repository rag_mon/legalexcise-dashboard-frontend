import API from './api';
import * as $ from 'jquery';


export default ($modalAddressDeleteConfirm, $modalSubmitMessage, $table) => {

    const lexicon = {

        RU: {
            button: {
                updates: 'Издать обновление',
            },
            modalSubmit: {
                success: {
                    title: 'Успех',
                    message: (ADDRESS) => `Адрес <b>${ADDRESS}</b> успешно удален.`,
                },
                error: {
                    title: 'Ошибка',
                    message: `Произошла непредвиденная ошибка. Попробуйте перезагрузить страницу и повторить запрос.`,
                },
            }

        },
        UK: {
            button: {
                updates: 'Видати оновлення',
            },
            modalSubmit: {
                success: {
                    title: 'Успіх',
                    message: (ADDRESS) => `Адреса <b>${ADDRESS}</b> видалений.`,
                },
                error: {
                    title: 'Помилка',
                    message: `Сталася неочікувана помилка. Спробуйте перезавантажити сторінку і повторити запит.`,
                },
            }
        }
    };

    let currentLocal = ( window.config && window.config.lang) || 'RU';
    currentLocal = currentLocal.toUpperCase();

    if (currentLocal !== 'RU' || currentLocal !== 'UK') {
        currentLocal = 'RU';
    }


    $('.dropdown-item-delete').unbind('click').bind('click', function (e) {
        e.preventDefault();

        const ID = $(this).attr('data-address-id');
        const ADDRESS = $(this).attr('data-title');
        console.log(API);
        console.log(ID);

        $modalAddressDeleteConfirm.find('.address-value').text(ADDRESS);


        $modalAddressDeleteConfirm.find('.btn-delete').unbind('click').bind('click', e => {


            $modalAddressDeleteConfirm.modal('hide');

            // $modalPreLoader.modal('show');


            API.deleteAddress(ID)
                .then((response) => {
                    console.log(response);
                    if($table){
                        $table.ajax.reload(function (data) {
                            console.log('RELOAD TABLES!',data)
                        }, false);
                    }
                    $modalSubmitMessage.find('#modalSubmitMessageLabel').html(lexicon[currentLocal].modalSubmit.success.title);
                    $modalSubmitMessage.find('.data-message-value').html(lexicon[currentLocal].modalSubmit.success.message(ADDRESS));
                    $modalSubmitMessage.modal('show');

                    // если переменная таблицы не инициализирована, то значит мы находимся на странице "управления адреса"
                    // в таком случае нужно редиректить на страницу (url редиректа передаётся с backend-а) после успешного удаления
                    if (!$table) {
                        let redirectUrl = $(this).attr('data-redirect');
                        setTimeout(() => window.location.replace(redirectUrl), 3000);
                    }
                })
                .catch(err => {
                    console.error(err);
                    // $modalPreLoader.modal('hide');
                    $modalSubmitMessage.find('#modalSubmitMessageLabel').html(lexicon[currentLocal].modalSubmit.error.title);
                    $modalSubmitMessage.find('.data-message-value').html(lexicon[currentLocal].modalSubmit.error.message);
                    $modalSubmitMessage.modal('show');
                })
        });

    });
}

