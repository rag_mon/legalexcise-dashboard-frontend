import {
  required,
  isNumber,
  Latitude,
  Longtude,
  maxLength,
} from './validation';

export const formConfig = [
  {
    id: 'inputINN',
    name: 'id_code',
    required: false,
    validate: [isNumber, maxLength(11)]
  }, {
    id: 'inputAddress',
    name: 'address',
    required: true,
    validate: [required, maxLength(1000)]
  }, {
    id: 'inputLicenseCode',
    name: 'license',
    required: false,
    validate: [isNumber]
  }, {
    id: 'inputStatus',
    name: 'status',
    required: false,
    validate: []
  }, {
    id: 'inputLat',
    name: 'lat',
    required: false,
    validate: [Latitude]
  }, {
    id: 'inputLong',
    name: 'lng',
    required: false,
    validate: [Longtude]
  }, {
    id: 'inputObjectType',
    name: 'company_type',
    required: false,
    validate: []
  }, {
    id: 'inputCompany',
    name: 'company',
    required: true,
    validate: [required]
  }, {
    id: 'inputLicenseStartAt',
    name: 'license_start_at',
    required: false,
    validate: []
  }, {
    id: 'inputLicenseEndAt',
    name: 'license_end_at',
    required: false,
    validate: []
  }, {
    id: 'inputConfiscatedGoods',
    name: 'confiscated_goods',
    required: false,
    validate: []
  }, {
    id: 'inputProtocolDrawnUp',
    name: 'protocol_drawn_up',
    required: false,
    validate: []
  }, {
    id: 'inputFinancialSanctions',
    name: 'financial_sanctions',
    required: false,
    validate: []
  },
];
