import API from './api';
import * as $ from 'jquery';


export default ($modalAddressAddComment, $modalSubmitMessage, $table) => {

    const lexicon = {

        RU: {
            button: {
                updates: 'Издать обновление',
            },
            modalSubmit: {
                success: {
                    title: 'Успех',
                    message: (ADDRESS) => `Комментарий на адрес <b>“${ADDRESS}”</b> успешно добавлен`,
                },
                error: {
                    title: 'Ошибка',
                    message: `Произошла непредвиденная ошибка. Попробуйте перезагрузить страницу и повторить запрос.`,
                },
            }

        },
        UK: {
            button: {
                updates: 'Видати оновлення',
            },
            modalSubmit: {
                success: {
                    title: 'Успіх',
                    message: (ADDRESS) => `Коментар на адресу <b>“${ADDRESS}”</b> доданий`,
                },
                error: {
                    title: 'Помилка',
                    message: `Сталася неочікувана помилка. Спробуйте перезавантажити сторінку і повторити запит.`,
                },
            }
        }
    };

    let currentLocal = ( window.config && window.config.lang) || 'RU';
    currentLocal = currentLocal.toUpperCase();

    if (currentLocal !== 'RU' || currentLocal !== 'UK') {
        currentLocal = 'RU';
    }


    $('.dropdown-item-add-comment').unbind('click').bind('click', function (e) {
        e.preventDefault();
        const ID = $(this).attr('data-address-id');
        const ADDRESS = $(this).attr('data-title');
        console.log(ID);
        $modalAddressAddComment.find('.btn-add').bind('click', e => {
            const MESSAGE = $modalAddressAddComment.find('#addressCommentText').val();
            const $VALID_MESSAGE = $('#addressCommentValidMessage');
            let validationMessage = '';

            if (MESSAGE.length === 0) {
                validationMessage = 'Обязательно для заполнения!';
            } else if (MESSAGE.length >= 1000) {
                validationMessage = 'Поле может содержать максимум 1000 символов';
            }
            if (validationMessage) {
                $VALID_MESSAGE.text(validationMessage).removeClass('d-none');
            } else {
                $VALID_MESSAGE.addClass('d-none');
                $modalAddressAddComment.modal('hide');

                $modalAddressAddComment.find('.btn-add').unbind('click');
                $modalAddressAddComment.find('#addressCommentText').val('');
                API.addComment(ID, MESSAGE)
                    .then((response) => {
                        console.log(response);
                        if($table){
                            $table.ajax.reload(function (data) {
                                console.log('RELOAD TABLES!',data)
                            }, false);
                        }
                        $modalSubmitMessage.find('#modalSubmitMessageLabel').html(lexicon[currentLocal].modalSubmit.success.title);
                        $modalSubmitMessage.find('.data-message-value').html(lexicon[currentLocal].modalSubmit.success.message(ADDRESS));
                        $modalSubmitMessage.modal('show');
                    })
                    .catch(err => {
                        console.error(err);
                        $modalSubmitMessage.find('#modalSubmitMessageLabel').html(lexicon[currentLocal].modalSubmit.error.title);
                        $modalSubmitMessage.find('.data-message-value').html(lexicon[currentLocal].modalSubmit.error.message);
                        $modalSubmitMessage.modal('show');
                    })
            }


        });
    })

}
