import * as $ from 'jquery';
import dateFilter from '../datatable/plugins/dateFilter';
import DataTableAPI  from '../datatable/api';

import moment from 'moment';

export default (function () {

  // DATA SORT
  dateFilter($, moment);
  $.fn.dataTable.moment('DD.MM.YYYY HH:mm');

  const $addressListTable = $('#addressHistoryListTable')
    .on('init.dt', function () {
      console.log('Table initialisation complete');

      setTimeout(() => {
        $addressListTable.draw();
      }, 1000)
    })
    .DataTable({
    buttons: [
      'delete'
    ],
    "drawCallback": function(  ) {
      console.log('drawCallback');

    },
    stateSave: false,
    ordering: true,
    info: true,
    scrollX: true,
    "processing": true,
    "serverSide": true,
    "ajax": "/history",
    "columns": [
      {
        "render": function (data, type, row) {
          try{
            return row.user.data.name;
          } catch (err){
            console.error(err);
            // у нас возможно такое, что пользователь на событие может быть не задан
            // т.е. у нас возможны события, которые были вазываны системой
            // но, я для таких случаев, чтобы не усложнять тебе жизнь буду отдавать данные супер-пользователя
            // с той же структурой, что и сейчас пользователей
          }
        }
      },
      {
        "render": function (data, type, row) {
          // пока что не подключил гурппы/роли пользователей
          // заглушка
          return '-';
          // return row.type;
        }
      },
      {
        "render": function (data, type, row) {
          return row.message;
        }
      },
      {
        "render": function (data, type, row) {
          // заменено: created_at -> fixed_at
          return row.fixed_at;
        }
      },
    ],

      language: {
          url: DataTableAPI.getLanguageUrl(),
      }
  });

  $addressListTable.columns().every( function () {
    let that = this;

    $( 'input', this.footer() ).on( 'keyup change', function () {
      if ( that.search() !== this.value ) {
        console.log(this.value);
        that
          .search( this.value )
          .draw();
      }
    } );

    $( 'select', this.footer() ).on( 'keyup change', function () {
      if ( that.search() !== this.value ) {
        console.log(this.value);
        that
          .search( this.value )
          .draw();
      }
    } );

  } );

}());
