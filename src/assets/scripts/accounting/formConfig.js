export const formConfig = [
  {
    id: 'transferred_to_the_oy',
    name: 'transferred_to_the_oy',
    required: true,
    validate: []
  },
  {
    id: 'sale_to_minors',
    name: 'sale_to_minors',
    required: true,
    validate: []
  },
  {
    id: 'examination_of_excisable_goods',
    name: 'examination_of_excisable_goods',
    required: true,
    validate: []
  },
];
