import * as $ from 'jquery';
import {fetchAPI} from '../utils/fetch';


export default {

  createAccounting(data) {
    return new Promise((resolve, reject) => {
      fetchAPI(`/hack/anal/common`, 'POST',data)
        .then((response) => {
          return response.json()
        })
        .then((response) => {
          resolve(response);
        })
        .catch((err) => {
          reject(err);
        });
    })
  },
}
