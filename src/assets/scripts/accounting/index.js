import * as $ from 'jquery';
import API from './api';
import {formConfig} from './formConfig';
import {findFalse} from "../address/validation";


export default (function () {


  const $form = $('#formAccountingEdit');
  const $formError = $('#formError')
  const $modalSubmitMessage = $('#modalSubmitMessage');
  const lexicon = {

    RU: {
      button: {
        updates: 'Издать обновление',
      },
      modalSubmit: {
        success: {
          title: 'Успех',
          message: (ADDRESS) => `Адрес <b>“${ADDRESS}”</b> удален`,
        },
        error: {
          title: 'Ошибка',
          message: `Произошла непредвиденная ошибка. Попробуйте перезагрузить страницу и повторить запрос.`,
        },
      }

    },
    UK: {
      button: {
        updates: 'Видати оновлення',
      },
      modalSubmit: {
        success: {
          title: 'Успіх',
          message: (ADDRESS) => `Адреса <b>“${ADDRESS}”</b> видалений`,
        },
        error: {
          title: 'Помилка',
          message: `Сталася неочікувана помилка. Спробуйте перезавантажити сторінку і повторити запит.`,
        },
      }
    }
  };

  let currentLocal = ( window.config && window.config.lang) || 'RU';
  currentLocal = currentLocal.toUpperCase();

  if (currentLocal !== 'RU' || currentLocal !== 'UK') {
    currentLocal = 'RU';
  }


  $form.submit(function (event) {
    console.log("Handler for .submit() called.");
    event.preventDefault();


    const DATA = {};
    const ERROR = [];
    formConfig.map((item) => {

      if (!$form.find(`[name="${item.name}"]`).val().length) {
        ERROR.push('Пусто');
      } else {
        DATA[item.name] = $form.find(`[name="${item.name}"]`).val();
      }

    });

    if (ERROR.length === Object.keys(formConfig).length) {
      $formError.removeClass('invisible').text('Форма пуста. Необходимо заполнить минимум одно поле.');
      return
    }

    $formError.addClass('invisible').text('');

    API.createAccounting(DATA)
      .then((response) => {
        console.log(response);
        $form[0].reset();
        $modalSubmitMessage.find('#modalSubmitMessageLabel').html(lexicon[currentLocal].modalSubmit.success.title);
        $modalSubmitMessage.find('.data-message-value').html(lexicon[currentLocal].modalSubmit.success.message);
        $modalSubmitMessage.modal('show');
      })
      .catch(err => {
        console.error(err);
        // $modalPreLoader.modal('hide');
        $modalSubmitMessage.find('#modalSubmitMessageLabel').html(lexicon[currentLocal].modalSubmit.error.title);
        $modalSubmitMessage.find('.data-message-value').html(lexicon[currentLocal].modalSubmit.error.message);
        $modalSubmitMessage.modal('show');
      })

  });

}())
