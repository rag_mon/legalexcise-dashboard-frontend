var express = require('express');
var app = express();

app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

app.get('/address', function (req, res) {
  res.setHeader('Content-Type', 'application/json');
  res.send(require('./address.json'));

});

app.get('/complaints', function (req, res) {
  res.setHeader('Content-Type', 'application/json');
  res.send(require('./complaints.json'));

});
app.get('/complaint', function (req, res) {
  res.setHeader('Content-Type', 'application/json');
  res.send(require('./complaint.json'));

});
app.get('/violations', function (req, res) {
  res.setHeader('Content-Type', 'application/json');
  res.send(require('./violations.json'));

});
app.get('/report', function (req, res) {
  res.setHeader('Content-Type', 'application/json');
  res.send(require('./report.json'));

});

app.get('/updates', function (req, res) {
  console.log('/updates');
  res.setHeader('Content-Type', 'application/json');
  res.send(require('./updates.json'));
});
app.get('/analytics', function (req, res) {
  console.log('/analytics');
  res.setHeader('Content-Type', 'application/json');
  res.send(require('./updates.json'));
});

app.get('/hack/anal/count/suspicious_to_active_addresses', function (req, res) {
  console.log('/hack/anal/count/suspicious_to_active_addresses');
  // res.sendStatus(200);
  res.send('1001')
});

app.listen(4000, function () {
  console.log('Example app listening on port 4000!');
});
