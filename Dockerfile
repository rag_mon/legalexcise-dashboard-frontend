FROM node:8.10

LABEL maintainer="Arthur Ragimov <ragimov.artyr@gmail.com>"

RUN apt-get update && \
    apt-get install -y git && \
    mkdir -p /var/www

WORKDIR /var/www

VOLUME /var/www

EXPOSE 3000

CMD ["yarn", "dev", "--port=3000"]