const
  path              = require('path'),
  manifest          = require('../manifest'),
  HtmlWebpackPlugin = require('html-webpack-plugin');

const titles = {
  'index': 'Dashboard',
  'blank': 'Blank',
  'buttons': 'Buttons',
  'calendar': 'Calendar',
  'charts': 'Charts',
  'chat': 'Chat',
  'compose': 'Compose',
  'datatable': 'Datatable',
  'email': 'Email',
  'forms': 'Forms',
  'google-maps': 'Google Maps',
  'signin': 'Signin',
  'signup': 'Signup',
  'ui': 'UI',
  'vector-maps': 'Vector Maps',
  '404': '404',
  '500': '500',
  'basic-table': 'Basic Table',

  /*-------------------------------------------------------------------------------------------------------------------
   *    Application pages
   *-----------------------------------------------------------------------------------------------------------------*/
  'homepage': 'Homepage',

  // Address control
  'address-edit': 'Address New/Edit',
  'address-view': 'Address View',
  'address-list': 'Address list view',

  // Complaint control
  'complaint-edit': 'Complaint edit',
  'complaint-list': 'Complaint list',
  'complaint-view': 'Complaint view',

  // History
  'history': 'History feed',

  // Report
  'report': 'Reports',

  // Analytics
  'analytics': 'Analytics',

  // Updates
  'updates': 'updates',
  'accounting': 'Accounting',

};

module.exports = Object.keys(titles).map(title => {
  return new HtmlWebpackPlugin({
    template: path.join(manifest.paths.src, `${title}.html`),
    path: manifest.paths.build,
    filename: `${title}.html`,
    inject: true,
    minify: {
      collapseWhitespace: true,
      minifyCSS: true,
      minifyJS: true,
      removeComments: true,
      useShortDoctype: true,
    },
  });
});
