// ------------------
// @Table of Contents
// ------------------

/**
 * + @Loading Dependencies
 * + @Common Loaders
 * + @Exporting Module
 */


// ---------------------
// @Loading Dependencies
// ---------------------

const
  manifest          = require('../manifest'),
  path              = require('path'),
  cssNext           = require('postcss-cssnext');


// ---------------
// @Common Loaders
// ---------------



const rule = {
  test: /\.(html)$/,
  use: {
    loader: 'html-loader',
    options: {
      attrs: [':data-src']
    }
  }
};

// -----------------
// @Exporting Module
// -----------------

module.exports = rule;
