module.exports = [
  require('./js'),
  require('./images'),
  require('./css'),
  require('./sass'),
  require('./fonts'),
  require('./svg'),
  require('./vue'),
];
